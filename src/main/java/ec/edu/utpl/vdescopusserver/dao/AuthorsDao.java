/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.dao;

import static ec.edu.utpl.vdescopusserver.dao.CountrysDao.getConnection;
import ec.edu.utpl.vdescopusserver.model.Author;
import ec.edu.utpl.vdescopusserver.model.Institution;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author Jonathan
 */
public class AuthorsDao {
    Connection connection = getConnection("jdbc:mysql://127.0.0.1:3307/scopus?user=root&password=1234");
    TreeMap<String, Integer> mapAuthors = new TreeMap<String, Integer>();
    
    public List<Author> authors(String country, String city, String institution, Integer cont) throws SQLException{
        List<Author> listAuthors = new ArrayList<Author>();
        listAuthors = getDBAuthors(country, city, institution,cont);
        return listAuthors;
    }

    private List<Author> getDBAuthors(String country, String city, String institution, Integer cont) throws SQLException {
       Statement stm = connection.createStatement();
        ResultSet rs = stm.executeQuery("select distinct a.auth_name as PName, a.given_name as Name, a.surname, a.initials\n"
                + "from afiliacion_auth_pub aap, afiliacion af, autor a, publicacion p\n"
                + "where aap.aff_id = af.aff_id\n"
                + "and aap.aut_id = a.id\n"
                + "and aap.pub_id = p.id\n"
                + "and af.aff_country = '"+country+"'\n"
                + "and af.aff_city = '"+city+"'\n"
                + "and af.aff_name = '"+institution+"';");
        List<Author> listAuth = new ArrayList<Author>();
        while(rs.next()){
            mapAuthors.put(rs.getString("PName"), cont);
            cont++;
            listAuth.add(new Author(rs.getString("Name"), rs.getString("surname"), rs.getString("initials"), rs.getString("PName"), 2));
        }
        return listAuth;
    }
    
}
