/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.dao;

import static ec.edu.utpl.vdescopusserver.dao.CountrysDao.getConnection;
import ec.edu.utpl.vdescopusserver.model.Links;
import ec.edu.utpl.vdescopusserver.model.Publication;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author jonathan
 */
public class LinksDao {
    Connection connection = getConnection("jdbc:mysql://127.0.0.1:3307/scopus?user=root&password=1234");    
    
    public List<Links> relacionar(String country, String city, String institution, Map<String, Integer> inst, Map<String, Integer> auth, Map<String, Integer> pub) throws SQLException{
        List<Links> listLinks = new ArrayList<Links>();
        listLinks = getDBRelations(country, city, institution, inst, auth, pub);
        return listLinks;
    }

    private List<Links> getDBRelations(String country, String city, String institution, Map<String, Integer> inst, Map<String, Integer> auth, Map<String, Integer> pub) throws SQLException {
       Statement stm = connection.createStatement();
        ResultSet rs = stm.executeQuery("select af.aff_name, a.auth_name, p.title\n"
                + "from afiliacion_auth_pub aap, afiliacion af, autor a, publicacion p\n"
                + "where aap.aff_id = af.aff_id\n"
                + "and aap.aut_id = a.id\n"
                + "and aap.pub_id = p.id\n"
                + "and af.aff_country = '"+country+"'\n"
                + "and af.aff_city = '"+city+"'\n"
                + "and af.aff_name = '"+institution+"';");
        List<Links> listLinks = new ArrayList<Links>();
        while(rs.next()){
            String aff_name, auth_name,title;
            aff_name = rs.getString("aff_name");
            auth_name = rs.getString("auth_name");
            title = rs.getString("title");
            
            //System.out.println("aff_name: "+aff_name+" auth_name: "+auth_name+" title: "+title);
            
            if (inst.containsKey(aff_name) && auth.containsKey(auth_name)) {
                System.out.println(":) "+auth_name);
                System.out.println(":D "+aff_name);
                listLinks.add(new Links(auth.get(auth_name), inst.get(aff_name), 1));
            }
            if (auth.containsKey(auth_name) && pub.containsKey(title)) {
                listLinks.add(new Links(pub.get(title), auth.get(auth_name), 1));
            }
        }
        return listLinks;
    }
    
}
