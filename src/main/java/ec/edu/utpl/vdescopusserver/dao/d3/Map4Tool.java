/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.dao.d3;

import com.hp.hpl.jena.sparql.lib.org.json.JSONArray;
import com.hp.hpl.jena.sparql.lib.org.json.JSONException;
import com.hp.hpl.jena.sparql.lib.org.json.JSONObject;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jonathan
 */
public class Map4Tool {
    
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    
    public Map<String, List<String>> d3Miserables(Map<String, List<String>> mapParameters) throws Exception{
        Map<String, List<String>> mapParChaged = new HashMap<String, List<String>>();
        //String pathEquivalences = "C:\\Users\\jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEParser\\src\\main\\java\\ec\\edu\\utpl\\vdeparser\\d3\\equivalenciasD3.json";
        URL pathEquivalences = classLoader.getResource("../json/equivalenciasD3.json");
        mapParChaged = parseGraphEquiv(mapParameters, pathEquivalences.getPath());
        
        for (Map.Entry<String, List<String>> entry : mapParChaged.entrySet()) {
            String string = entry.getKey();
            System.out.println("KEY: "+string);
            List<String> list = entry.getValue();
            for (String string1 : list) {
                System.out.println("VALUE: "+string1);
            }
            
        }
        
        return mapParChaged;
    }
    
    public Map<String, List<String>> dataVir(Map<String, List<String>> mapParameters) throws Exception{
        System.out.println("################# dataVir #####################");
        Map<String, List<String>> mapParChaged = new HashMap<String, List<String>>();
        //String pathEquivalences = "C:\\Users\\jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEScopusServerV\\src\\main\\java\\ec\\edu\\utpl\\vdescopusserver\\equivalencias.json";
        URL pathEquivalences =classLoader.getResource("../json/equivalenciasD3.json");
        mapParChaged = parseGraphEquiv(mapParameters, pathEquivalences.getPath());
        
        for (Map.Entry<String, List<String>> entry : mapParChaged.entrySet()) {
            String string = entry.getKey();
            System.out.println("KEY: "+string);
            List<String> list = entry.getValue();
            for (String string1 : list) {
                System.out.println("VALUE: "+string1);
            }
            
        }
        
        return mapParChaged;
    }
    
    public Map<String, List<String>> parseGraphEquiv(Map<String, List<String>> mapParChaged, String path) throws Exception {
        Map<String, List<String>> mapGraphEquiv = new HashMap<String, List<String>>();
        mapGraphEquiv = getPredicateJson(mapParChaged, path);
        return mapGraphEquiv;
    }
    
    private Map<String, List<String>> getPredicateJson(Map<String, List<String>> value, String pathJson) throws FileNotFoundException, JSONException {
        if (value != null && !value.isEmpty() && pathJson!=null && !pathJson.isEmpty()) {
            String path = pathJson;
            InputStream is = new FileInputStream(path);
            String json = getStringFromInputStream(is);
            JSONObject jsonObject = new JSONObject(json);
            JSONArray arr = jsonObject.names();
            String strArr [] = new String[arr.length()];
            String val;

            Map<String, List<String>> predicates = new HashMap<String, List<String>>();
            
            for (int j = 0; j < strArr.length; j++) {
                if (arr.getString(j).contains("#")) {
                    strArr[j] = getLastChar(arr.getString(j), "#");
                } else if (arr.getString(j).contains("/")) {
                    strArr[j] = getLastChar(arr.getString(j), "/");
                } else {
                    strArr[j] = arr.getString(j);
                }
            }
            
            List<String> str = new ArrayList<String>();
            String aux;
            
            for (int i = 0; i < arr.length(); i++) {
                
                if ((strArr[i].equals("name") || strArr[i].equals("legalName")) && value.containsKey("name")) {
                    for (int j = 0; j < value.get("name").size(); j++) {
                        str.add(value.get("name").get(j));
                        if (j==0) {
                            predicates.put("legalName", str);
                        } else{
                            predicates.put("name", str);
                        }
                    }
                    continue;
                }
                
                val = jsonObject.get(arr.getString(i)).toString().replace("[", "").replace("]", "").replace("\"", "").trim();
                if (val!=null && !val.isEmpty()) {
                    if (value.containsKey(val)) {
                        predicates.put(arr.getString(i), value.get(val));
                    }
                }
                if (value.containsKey(strArr[i])) {
                    predicates.put(arr.getString(i), value.get(strArr[i]));
                }
            }
            return predicates;
        }
        
        return null;
    }
    
    private static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
     }
    
    private static String getLastChar(String string, String split) {
        int ic=0;
        String val="";
        ic = string.lastIndexOf(split);
        val = string.substring(ic+1, string.length());
        return val;
    }
}
