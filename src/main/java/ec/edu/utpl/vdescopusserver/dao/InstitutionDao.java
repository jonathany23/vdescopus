/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.dao;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.Model;
import ec.edu.utpl.util.VDEConstant;
import static ec.edu.utpl.vdescopusserver.dao.CountrysDao.getConnection;
import static ec.edu.utpl.vdescopusserver.dao.CountrysDao.url;
import ec.edu.utpl.vdescopusserver.model.Country;
import ec.edu.utpl.vdescopusserver.model.Institution;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtModel;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 *
 * @author jonathan
 */
public class InstitutionDao {
    
    public static VirtuosoQueryExecution vur;
    public static final String graph = VDEConstant.VIRT_GRAPH;
    static String url;
    static String user;
    static String password;
    
    Connection connection = getConnection("jdbc:mysql://127.0.0.1:3307/scopus?user=root&password=1234");
    TreeMap<String, Integer> mapInst = new TreeMap<String, Integer>();
    
    public List<Institution> institution(String country, String city, String institution) throws SQLException{
        List<Institution> listInst = new ArrayList<Institution>();
        listInst = getDBInstitution(country, city, institution);
        return listInst;
    }

    private List<Institution> getDBInstitution(String country, String city, String institution) throws SQLException {
//       Statement stm = connection.createStatement();
//        ResultSet rs = stm.executeQuery("select distinct af.aff_name as ins, af.aff_country as country, af.aff_city as city\n"
//                + "from afiliacion_auth_pub aap, afiliacion af, autor a, publicacion p\n"
//                + "where aap.aff_id = af.aff_id\n"
//                + "and aap.aut_id = a.id\n"
//                + "and aap.pub_id = p.id\n"
//                + "and af.aff_country = '" + country + "'\n"
//                + "and af.aff_city = '" + city + "'\n"
//                + "and af.aff_name = '" + institution + "';");
        List<Institution> listInst = new ArrayList<Institution>();
//        Integer cont = 0;
//        while(rs.next()){
//            mapInst.put(rs.getString("ins"), cont);
//            listInst.add(new Institution(rs.getNString("country"), rs.getNString("city"), rs.getNString("ins"), 1));
//            cont++;
//        }
//        return listInst;
        VirtGraph set;
            set = new VirtGraph (VDEConstant.VIRT_SERVER, VDEConstant.VIRT_USER, VDEConstant.VIRT_PASS);
            url = VDEConstant.VIRT_SERVER;
            user = VDEConstant.VIRT_USER;
            password = VDEConstant.VIRT_PASS;
            set.clear();
            
            Model model = VirtModel.openDatabaseModel(graph, url, user, password);
        List<Country> listCountries = new ArrayList<Country>();
        VirtuosoQueryExecution vqm = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create("select ?s where { ?s <http://api.elsevier.com/content/affiliation/affiliation_id/aff_country> \""+country+"\"^^<http://www.w3.org/2001/XMLSchema#string> . ?s <http://api.elsevier.com/content/affiliation/affiliation_id/aff_city> \""+city+"\"^^<http://www.w3.org/2001/XMLSchema#string> . ?s <http://api.elsevier.com/content/affiliation/affiliation_id/aff_name> \""+institution+"\"^^<http://www.w3.org/2001/XMLSchema#string> }", model);
        com.hp.hpl.jena.query.ResultSet results = vqm.execSelect();
        
        //while (results.hasNext()) {
            QuerySolution result = results.nextSolution();
            String id = result.get("s").toString();
            System.out.println("id: " + id);
        //}
            vqm.close();
        
        VirtuosoQueryExecution geoData = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create("select ?p ?o where { <"+id+"> ?p ?o filter(?p = <http://api.elsevier.com/content/affiliation/affiliation_id/aff_country> or ?p = <http://api.elsevier.com/content/affiliation/affiliation_id/aff_city> or  ?p = <http://api.elsevier.com/content/affiliation/affiliation_id/aff_name>)}", model);
        com.hp.hpl.jena.query.ResultSet resultsGeo = geoData.execSelect();
        
        TreeMap<String, String> mapInst = new TreeMap<String, String>();
        
        while (resultsGeo.hasNext()) {
            QuerySolution resultGeo = resultsGeo.nextSolution();
            mapInst.put(resultGeo.get("p").toString(), resultGeo.get("o").toString());            
        }
        
        String p="", c="", i="";
        for (Map.Entry<String, String> entry : mapInst.entrySet()) {
            if (entry.getKey().contains("country"))
                p = entry.getValue();
            else if (entry.getKey().contains("city"))
                c = entry.getValue();
            else if (entry.getKey().contains("name"))
                i = entry.getValue();
        }
        listInst.add(new Institution(p, c, i, 1));
        
//        iterResult(results);
        return listInst;
    }
    
//    public List iterResult(com.hp.hpl.jena.query.ResultSet results) {
//    }
}
