/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.dao;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.Model;
import ec.edu.utpl.util.VDEConstant;
import ec.edu.utpl.vde.exceptions.ConectionTimeOutException;
import ec.edu.utpl.vdescopusserver.model.Country;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtModel;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 *
 * @author jonathan
 */
public class CountrysDao {
    
    public static VirtuosoQueryExecution vur;
    public static final String graph = VDEConstant.VIRT_GRAPH;
    static String url;
    static String user;
    static String password;
    
    public List<String> getCountries() throws Exception{
        Country co = new Country();
        getDBCountries(co);
        return co.getCountrys();
    }
    
    public List<String> getCities(String country) throws SQLException{
        Country co = new Country();
        getDBCities(co, country);
        return co.getCountrys();
    }
    
    public List<String> getInstitutes(String country, String city) throws SQLException{
        Country co = new Country();
        getDBInstitutes(co, country, city);
        return co.getCountrys();
    }

    private void getDBCountries(Country co) throws Exception {
        VirtGraph set;
        try {
            set = new VirtGraph (VDEConstant.VIRT_SERVER, VDEConstant.VIRT_USER, VDEConstant.VIRT_PASS);
            url = VDEConstant.VIRT_SERVER;
            user = VDEConstant.VIRT_USER;
            password = VDEConstant.VIRT_PASS;
            set.clear();
        } catch (Exception e) {
            throw new ConectionTimeOutException("Error Conectarse a Virtuoso, Por favor revise su configuracion e intente nuevamente.");
        }
        //Connection connection = getConnection("jdbc:mysql://127.0.0.1:3307/scopus?user=root&password=1234");
        //Statement stm = connection.createStatement();
        //ResultSet rs = stm.executeQuery("SELECT DISTINCT AF.aff_country AS COUNTRY FROM afiliacion AF ORDER BY COUNTRY;");
            Model model = VirtModel.openDatabaseModel(graph, url, user, password);
        List<Country> listCountries = new ArrayList<Country>();
        //String sprql = "select DISTINCT ?z where {?x <http://www.loc.gov/mads/rdf/v1#country> ?z} ORDER BY ?z";
        String sprql = "select distinct (?o)\n" +
"where {\n" +
"?s <http://www.loc.gov/mads/rdf/v1#country> ?o\n" +
"}\n" +
"order by ?o";
        VirtuosoQueryExecution vqm = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create(sprql, model);
        com.hp.hpl.jena.query.ResultSet results = vqm.execSelect();

        String pre = "";
        while (results.hasNext()) {
            QuerySolution result = results.nextSolution();
            //listCountries.add(Integer.parseInt(getLastValue(result.get("x").toString(), "/")));
            //String country [] = result.get("z").toString().split("\\^");
            pre = result.get("o").toString();
            co.setCountrys(pre);
            
//            if (country[0]!=null) {
//                //System.out.println(country[0]);
//                co.setCountrys(country[0]);
//            }
        }
//        while (rs.next()) {            
//            co.setCountrys(rs.getString("COUNTRY"));
//        }
    }
    
    private void getDBCities(Country co, String country) throws SQLException {
        VirtGraph set;
            set = new VirtGraph (VDEConstant.VIRT_SERVER, VDEConstant.VIRT_USER, VDEConstant.VIRT_PASS);
            url = VDEConstant.VIRT_SERVER;
            user = VDEConstant.VIRT_USER;
            password = VDEConstant.VIRT_PASS;
            set.clear();
            Model model = VirtModel.openDatabaseModel(graph, url, user, password);
        VirtuosoQueryExecution vqm = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create("select distinct (?o)\n" +
"where {\n" +
"?s <http://www.loc.gov/mads/rdf/v1#country> \""+country+"\" .\n" +
"?s <http://www.loc.gov/mads/rdf/v1#city> ?o\n" +
"}\n" +
"order by ?o", model);
        com.hp.hpl.jena.query.ResultSet results = vqm.execSelect();

        while (results.hasNext()) {
            QuerySolution result = results.nextSolution();
            String cities [] = result.get("o").toString().split("\\^");
            if (cities[0]!=null) {
                co.setCountrys(cities[0]);
            }
        }
        
        
//        Connection connection = getConnection("jdbc:mysql://127.0.0.1:3307/scopus?user=root&password=1234");
//        Statement stm = connection.createStatement();
//        ResultSet rs = stm.executeQuery("SELECT DISTINCT AF.aff_city AS CITY FROM afiliacion AF WHERE AF.aff_country = '"+country+"' ORDER BY CITY;");
//        while (rs.next()) {
//            co.setCountrys(rs.getString("CITY"));
//        }
    }
    
    private void getDBInstitutes(Country co, String country, String city) throws SQLException {
        VirtGraph set;
            set = new VirtGraph (VDEConstant.VIRT_SERVER, VDEConstant.VIRT_USER, VDEConstant.VIRT_PASS);
            url = VDEConstant.VIRT_SERVER;
            user = VDEConstant.VIRT_USER;
            password = VDEConstant.VIRT_PASS;
            set.clear();
            Model model = VirtModel.openDatabaseModel(graph, url, user, password);
        VirtuosoQueryExecution vqm = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create("select distinct (?o)\n" +
"where {\n" +
"?s <http://www.loc.gov/mads/rdf/v1#country> \""+country+"\" .\n" +
"?s <http://www.loc.gov/mads/rdf/v1#city> \""+city+"\" .\n" +
"?s <http://schema.org/legalName> ?o .\n" +
"}\n"
, model);
        com.hp.hpl.jena.query.ResultSet results = vqm.execSelect();

        while (results.hasNext()) {
            QuerySolution result = results.nextSolution();
            String cities [] = result.get("o").toString().split("\\^");
            if (cities[0]!=null) {
                co.setCountrys(cities[0]);
            }
        }
        
//        Connection connection = getConnection("jdbc:mysql://127.0.0.1:3307/scopus?user=root&password=1234");
//        Statement stm = connection.createStatement();
//        ResultSet rs = stm.executeQuery("SELECT DISTINCT AF.aff_name AS INS FROM afiliacion AF WHERE AF.aff_country = '"+country+"' AND AF.aff_city = '"+city+"' ORDER BY INS;");
//        while (rs.next()) {
//            co.setCountrys(rs.getString("INS"));
//        }
    }
    
    public static Connection getConnection(String uriconnection){
        Connection conexion = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String jdbcutf8 = "&useUnicode=true&characterEncoding=UTF-8";
            conexion = DriverManager.getConnection(uriconnection+jdbcutf8);
            
        } catch (ClassNotFoundException ex) {
            System.out.println("error conexion1 :"+ex.getCause());
        } catch (SQLException ex){
            System.out.println("error conexion2 :"+ex.getCause());
        }
        return conexion;

    }
}
