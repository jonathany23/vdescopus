/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.dao;

import static ec.edu.utpl.vdescopusserver.dao.CountrysDao.getConnection;
import ec.edu.utpl.vdescopusserver.model.Author;
import ec.edu.utpl.vdescopusserver.model.Publication;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author Jonathan
 */
public class PublicationDao {
    Connection connection = getConnection("jdbc:mysql://127.0.0.1:3307/scopus?user=root&password=1234");
    TreeMap<String, Integer> mapPub = new TreeMap<String, Integer>();
    
    public List<Publication> publications(String country, String city, String institution, Integer cont) throws SQLException{
        List<Publication> listPublications = new ArrayList<Publication>();
        listPublications = getDBPublications(country, city, institution, cont);
        return listPublications;
    }

    private List<Publication> getDBPublications(String country, String city, String institution, Integer cont) throws SQLException {
       Statement stm = connection.createStatement();
       ResultSet rs = stm.executeQuery("select distinct p.title as name, p.coverDate as fecha, p.publicationName as seccion, p.volume, p.authkeywords as keywords, p.description\n"
                + "from afiliacion_auth_pub aap, afiliacion af, autor a, publicacion p\n"
                + "where aap.aff_id = af.aff_id\n"
                + "and aap.aut_id = a.id\n"
                + "and aap.pub_id = p.id\n"
                + "and af.aff_country = '"+country+"'\n"
                + "and af.aff_city = '"+city+"'\n"
                + "and af.aff_name = '"+institution+"';");
        List<Publication> listPubli = new ArrayList<Publication>();
        while(rs.next()){
            mapPub.put(rs.getString("name"), cont);
            cont++;
            listPubli.add(new Publication(rs.getString("fecha"), rs.getString("seccion"), rs.getString("volume"), rs.getString("keywords"), rs.getString("description"), rs.getString("name"), 3));
        }
        return listPubli;
    }
}
