/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.dao;

import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.sparql.lib.org.json.JSONArray;
import com.hp.hpl.jena.sparql.lib.org.json.JSONException;
import com.hp.hpl.jena.sparql.lib.org.json.JSONObject;
import ec.edu.utpl.util.VDEConstant;
import static ec.edu.utpl.vdescopusserver.dao.CountrysDao.url;
import ec.edu.utpl.vdescopusserver.model.Pattern;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import virtuoso.jena.driver.VirtGraph;
import virtuoso.jena.driver.VirtModel;
import virtuoso.jena.driver.VirtuosoQueryExecution;
import virtuoso.jena.driver.VirtuosoQueryExecutionFactory;

/**
 *
 * @author Jonathan
 */
public class Source {

    public static final String graph = VDEConstant.VIRT_GRAPH;
    private Map<String, Map<String, Object>> query = new LinkedHashMap<String, Map<String, Object>>();
    private Map<String, Integer> mapLink = new LinkedHashMap<String, Integer>();
    static String user;
    static String password;
    
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

    public Pattern queryM(String country, String city, String institute) throws FileNotFoundException, JSONException {
        VirtGraph set;
        set = new VirtGraph(VDEConstant.VIRT_SERVER, VDEConstant.VIRT_USER, VDEConstant.VIRT_PASS);
        url = VDEConstant.VIRT_SERVER;
        user = VDEConstant.VIRT_USER;
        password = VDEConstant.VIRT_PASS;
        set.clear();
        
        URL pathEQ = classLoader.getResource("../json/equivalencias.json");

        Model model = VirtModel.openDatabaseModel(graph, url, user, password);
        List<List> lstRsp = new ArrayList<List>();
        List<Map<String, Object>> listNod = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> listLks = new ArrayList<Map<String, Object>>();
        String sprql = "select ?s ?p ?o\n"
                + "where {{ \n"
                + "?s <http://schema.org/affiliation> ?s1.\n"
                + "?s ?p ?o.\n"
                + "{select ?s1\n"
                + "where {\n"
                + "?s1 <http://www.loc.gov/mads/rdf/v1#country> \"" + country + "\" .\n"
                + "?s1 <http://www.loc.gov/mads/rdf/v1#city> \"" + city + "\" .\n"
                + "?s1 <http://schema.org/legalName> \"" + institute + "\".\n"
                + "}}\n"
                + "}\n"
                + "union\n"
                + "{\n"
                + "?s <http://www.loc.gov/mads/rdf/v1#country> \"" + country + "\" .\n"
                + "?s <http://www.loc.gov/mads/rdf/v1#city> \"" + city + "\" .\n"
                + "?s <http://schema.org/legalName> \"" + institute + "\".\n"
                + "?s ?p ?o .\n"
                + "}\n"
                + "union {\n"
                + "?s <http://schema.org/creator> ?s2.\n"
                + "?s ?p ?o.\n"
                + "{\n"
                + "      select ?s2\n"
                + "where {\n"
                + "   ?s2 <http://schema.org/affiliation> ?s1\n"
                + "   {\n"
                + "      select ?s1\n"
                + "      where {\n"
                + "         ?s1 <http://www.loc.gov/mads/rdf/v1#country> \"" + country + "\" .\n"
                + "         ?s1 <http://www.loc.gov/mads/rdf/v1#city> \"" + city + "\" .\n"
                + "         ?s1 <http://schema.org/legalName> \"" + institute + "\".\n"
                + "      }\n"
                + "    }\n"
                + "}\n"
                + "\n"
                + "}\n"
                + "}\n"
                + "}\n"
                + "order by ?s ?p";
        VirtuosoQueryExecution vqm = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create(sprql, model);
        com.hp.hpl.jena.query.ResultSet results = vqm.execSelect();

        Random rdn = new Random();
        String s = "", p = "", o = "", aux = "", color = "";
        int cont = 1, group = 0;
        double size;
        color = "rgb(" + (int) (rdn.nextDouble() * 255 + 0) + "," + (int) (rdn.nextDouble() * 255 + 0) + "," + (int) (rdn.nextDouble() * 255 + 0) + ")";
        Map<String, Object> preObj = new LinkedHashMap<String, Object>();
        Map<String, Integer> mapGroup = new LinkedHashMap<String, Integer>();
        Map<String, String> mapColor = new LinkedHashMap<String, String>();
        Map<String, Double> mapSize = new LinkedHashMap<String, Double>();
        List<String> pila = new ArrayList<String>();
        while (results.hasNext()) {
            QuerySolution result = results.nextSolution();
            s = result.get("s").toString();
            p = result.get("p").toString();
            o = result.get("o").toString();

            if (!s.equals(aux)) {
                preObj = new LinkedHashMap<String, Object>();
                pila.clear();
                cont = 1;
            }

            if (p != null && !p.isEmpty()) {
                if (pila.contains(p)) {
                    p += cont;
                    cont++;
                }
                preObj.put(p, o);
                if (p.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) {
                    if (mapGroup.containsKey(o)) {
                        preObj.put("group", mapGroup.get(o));
                        preObj.put("color", mapColor.get(o));
                        preObj.put("size", mapSize.get(o));
                    } else {
                        group++;
                        color = "rgb(" + (int) (rdn.nextDouble() * 255 + 0) + "," + (int) (rdn.nextDouble() * 255 + 0) + "," + (int) (rdn.nextDouble() * 255 + 0) + ")";
                        size = (rdn.nextDouble() * 10 + 1);
                        mapGroup.put(o, group);
                        mapColor.put(o, color);
                        mapSize.put(o, size);
                        preObj.put("group", group);
                        preObj.put("color", color);
                        preObj.put("size", size);
                    }
                }
                pila.add(p);
            }

            query.put(s, preObj);
            aux = s;
        }

        cont = 0;
        for (Map.Entry<String, Map<String, Object>> entry : query.entrySet()) {
            mapLink.put(entry.getKey(), cont);
            entry.getValue().put("id", String.valueOf(cont));
            entry.getValue().put("x", (rdn.nextDouble() * 1000 + 1));
            entry.getValue().put("y", (rdn.nextDouble() * 1000 + 1));
            //entry.getValue().put("size", (rdn.nextDouble() * 10 +1));
            cont++;
        }

        Map<String, Object> str;
        //Map<String, String> lks = new LinkedHashMap<String, String>();
        for (Map.Entry<String, Map<String, Object>> entry : query.entrySet()) {
            str = new LinkedHashMap<String, Object>();
            //lks = new LinkedHashMap<String, String>();

            str = replacePredicate(entry.getValue(), pathEQ);
            listNod.add(str);
            //listLks.add(lks);
        }
        listLks = getLinks();

        lstRsp.add(listNod);
        lstRsp.add(listLks);

        Pattern pat = new Pattern();
        pat.setNodes(listNod);
        pat.setLinks(listLks);

        return pat;
    }
    
    public Pattern queryCountries()throws Exception{
        VirtGraph set;
        set = new VirtGraph(VDEConstant.VIRT_SERVER, VDEConstant.VIRT_USER, VDEConstant.VIRT_PASS);
        url = VDEConstant.VIRT_SERVER;
        user = VDEConstant.VIRT_USER;
        password = VDEConstant.VIRT_PASS;
        set.clear();
        
        URL pathEQ = classLoader.getResource("../json/EQCountries.json");
        

        Model model = VirtModel.openDatabaseModel(graph, url, user, password);
//        List<List> lstRsp = new ArrayList<List>();
        List<Map<String, Object>> listNod = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> listLks = new ArrayList<Map<String, Object>>();
        String sprql = "select distinct(?o) as ?country count(?o) as ?publications\n" +
"where {\n" +
"?s <http://www.loc.gov/mads/rdf/v1#country> ?o\n" +
"}\n" +
"order by ?o";
        VirtuosoQueryExecution vqm = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create(sprql, model);
        com.hp.hpl.jena.query.ResultSet results = vqm.execSelect();
        
        String country, numPubli, p="http://www.loc.gov/mads/rdf/v1#country";
        Map<String, Object> nodes = null;
        int max=0;
        
        while (results.hasNext()) {
            QuerySolution result = results.nextSolution();
            country = result.get("country").toString();
            numPubli = result.get("publications").toString();
            
            if(numPubli.contains("^"))
                numPubli = getFirstChar(numPubli, "^");
            
            //verifica el numero maximo y minimo de publicaciones por pais
            //para sacar un valor en porcentajes y poder determinar el tamaño grupo de la grafica
            int numP = Integer.parseInt(numPubli);            
            max = numP > max ? numP : max;
            
            nodes = new LinkedHashMap<String, Object>();
            nodes.put(p, country);
            nodes.put("numPublications", numP);
            if (pathEQ != null)
                nodes = replacePredicate(nodes, pathEQ);
            else
                System.out.println("Error al leer archivo de equivalencias");
            listNod.add(nodes);
            nodes =null;
        }
        
        double r=0;
        for (Map<String, Object> map : listNod) {
            r=Math.ceil(((Integer) map.get("numPublications")*100)/max);
            map.put("r", r < 5 ? 5: r);
            map.put("group", Math.ceil(((Integer) map.get("numPublications")*10)/max));
        }
        
        Pattern pat = new Pattern();
        pat.setNodes(listNod);
        return pat;
    }

    private Map<String, Object> replacePredicate(Map<String, Object> value, URL pathEQ) throws FileNotFoundException, JSONException {
        //String path = "C:\\Users\\Jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEScopusServerV\\src\\main\\java\\ec\\edu\\utpl\\vdescopusserver\\equivalencias.json";
        //InputStream is = new FileInputStream(path);
        InputStream is = new FileInputStream(pathEQ.getPath());
        String json = getStringFromInputStream(is);
        JSONObject jsonObject = new JSONObject(json);
        JSONArray arr = jsonObject.names();
        String val;
        boolean isNames = false;

        Map<String, Object> predicates = new HashMap<String, Object>();
        for (Map.Entry<String, Object> entry : value.entrySet()) {
            for (int i = 0; i < arr.length(); i++) {
                if (entry.getKey().equals(arr.getString(i))) {
                    isNames = true;
                    val = jsonObject.get(arr.getString(i)).toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\\"", "");
                    if (!val.isEmpty()) {
                        predicates.put(val, entry.getValue());
                    } else {
                        if (entry.getKey().contains("#")) {
                            predicates.put(getLastChar(entry.getKey(), "#"), entry.getValue());
                        } else if ((entry.getKey().contains("/"))) {
                            predicates.put(getLastChar(entry.getKey(), "/"), entry.getValue());
                        } else {
                            predicates.put(entry.getKey(), entry.getValue());
                        }
                    }
                    break;
                }
            }
            if (!isNames) {
                if (entry.getKey().contains("#")) {
                    predicates.put(getLastChar(entry.getKey(), "#"), entry.getValue());
                } else if (entry.getKey().contains("/")) {
                    predicates.put(getLastChar(entry.getKey(), "/"), entry.getValue());
                } else {
                    predicates.put(entry.getKey(), entry.getValue());
                }
            }
            isNames = false;
        }

        return predicates;
    }

    private static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    private static String getLastChar(String string, String split) {
        int ic = 0;
        String val = "";
        ic = string.lastIndexOf(split);
        val = string.substring(ic + 1, string.length());
        return val;
    }
    
    private static String getFirstChar(String string, String split) {
        int ic = 0;
        String val = "";
        ic = string.indexOf(split);
        val = string.substring(0, ic);
        return val;
    }

    private List getLinks() {
        Map<String, Object> linkMap = new HashMap<String, Object>();
        List<Map<String, Object>> lstLks = new ArrayList<Map<String, Object>>();
        boolean isAdded = false;
        int cont = 0;
        for (Map.Entry<String, Map<String, Object>> entry0 : query.entrySet()) {
            String key = entry0.getKey();
            for (Map.Entry<String, Map<String, Object>> entry : query.entrySet()) {
                if (!key.equals(entry.getKey())) {
                    for (Map.Entry<String, Object> entry1 : entry.getValue().entrySet()) {
                        if (key.equals(entry1.getValue())) {
                            linkMap = new HashMap<String, Object>();
                            linkMap.put("source", mapLink.get(entry.getKey()));
                            linkMap.put("target", mapLink.get(key));
                            linkMap.put("id", String.valueOf(cont));
                            isAdded = true;
                            cont++;
                            break;
                        }
                    }
                    if (isAdded) {
                        lstLks.add(linkMap);
                    }
                    isAdded = false;
                }

            }
        }
        return lstLks;
    }

    public Pattern queryParam(Map<String, List<String>> mapParamGraph) throws Exception {
        VirtGraph set;
        set = new VirtGraph(VDEConstant.VIRT_SERVER, VDEConstant.VIRT_USER, VDEConstant.VIRT_PASS);
        url = VDEConstant.VIRT_SERVER;
        user = VDEConstant.VIRT_USER;
        password = VDEConstant.VIRT_PASS;
        set.clear();
        
        URL pathEQ = classLoader.getResource("../equivalencias.json");

        List<String> institutions = new ArrayList<String>();
        institutions = Arrays.asList("http://schema.org/membershipNumber", "http://schema.org/url", /*"http://www.loc.gov/mads/rdf/v1#city",
                 "http://www.loc.gov/mads/rdf/v1#country", "http://schema.org/legalName",*/ "http://schema.org/productID");

        List<String> authors = new ArrayList<String>();
        authors = Arrays.asList("http://schema.org/membershipNumber", "http://schema.org/url", "http://schema.org/productID",
                "http://schema.org/name", "http://schema.org/givenName", "http://schema.org/additionalName", "http://schema.org/familyName", "http://schema.org/affiliation");

        List<String> publications = new ArrayList<String>();
        publications = Arrays.asList("http://schema.org/membershipNumber", "http://schema.org/url", "http://schema.org/productID",
                "http://schema.org/name", "http://schema.org/learningResourceType", "http://schema.org/keywords\n", "http://schema.org/interactionCount",
                "http://schema.org/dateline", "http://schema.org/creator", "http://schema.org/description", "http://schema.org/text",
                "http://schema.org/alternativeHeadline", "http://schema.org/interactivityType", "http://schema.org/alternateName", "http://schema.org/version");

        StringBuilder sbQuery = new StringBuilder();
        StringBuilder sbInstitution = new StringBuilder();
        StringBuilder sbAuthor = new StringBuilder();
        StringBuilder sbPublication = new StringBuilder();
        StringBuilder sbDetInst = new StringBuilder();
        StringBuilder sbDetInst0 = new StringBuilder();
        StringBuilder sbAuthor2 = new StringBuilder();

        String country = "", city = "", institute = "";

        if (mapParamGraph.get("http://www.loc.gov/mads/rdf/v1#country") != null && !mapParamGraph.get("http://www.loc.gov/mads/rdf/v1#country").isEmpty()) {
            country = mapParamGraph.get("http://www.loc.gov/mads/rdf/v1#country").get(0);
        }
        if (mapParamGraph.get("http://www.loc.gov/mads/rdf/v1#city") != null && !mapParamGraph.get("http://www.loc.gov/mads/rdf/v1#city").isEmpty()) {
            city = mapParamGraph.get("http://www.loc.gov/mads/rdf/v1#city").get(0);
        }
        if (mapParamGraph.get("http://schema.org/legalName") != null && !mapParamGraph.get("http://schema.org/legalName").isEmpty()) {
            institute = mapParamGraph.get("http://schema.org/legalName").get(0);
        }

        sbDetInst.append("{\n");
        sbDetInst.append("select ?s1\n");
        sbDetInst.append("where {\n");
        sbDetInst.append("?s1 <http://www.loc.gov/mads/rdf/v1#country> \"" + country + "\" .\n");
        sbDetInst.append("?s1 <http://www.loc.gov/mads/rdf/v1#city> \"" + city + "\" .\n");
        sbDetInst.append("?s1 <http://schema.org/legalName> \"" + institute + "\".\n");
        sbDetInst.append("}\n");
        sbDetInst.append("}\n");

        sbDetInst0.append("?s <http://www.loc.gov/mads/rdf/v1#country> \"" + country + "\" .\n");
        sbDetInst0.append("?s <http://www.loc.gov/mads/rdf/v1#city> \"" + city + "\" .\n");
        sbDetInst0.append("?s <http://schema.org/legalName> \"" + institute + "\".\n");

        sbAuthor.append("?s <http://schema.org/affiliation> ?s1.\n");
        sbPublication.append("?s <http://schema.org/creator> ?s2.\n");
        for (Map.Entry<String, List<String>> entry : mapParamGraph.entrySet()) {
            String p = entry.getKey();
            List<String> s = entry.getValue();
            if (authors.contains(p)) {
                for (String litSub : s) {
                    sbAuthor.append("?s <").append(p).append("> \"").append(litSub).append("\" .\n");
                    sbAuthor2.append("?s2 <").append(p).append("> \"").append(litSub).append("\" .\n");
                }
            }
            if (institutions.contains(p)) {
                for (String litSub : s) {
                    sbInstitution.append("?s <").append(p).append("> \"").append(litSub).append("\" .\n");
                }
            }
            if (publications.contains(p)) {
                for (String litSub : s) {
                    sbPublication.append("?s <").append(p).append("> \"").append(litSub).append("\" .\n");
                }
            }
        }
        sbAuthor.append("?s ?p ?o.\n");
        sbInstitution.append("?s ?p ?o.\n");
        sbPublication.append("?s ?p ?o.\n");
        System.out.println("Query, Autores: "+sbAuthor2);

        sbQuery.append("select ?s ?p ?o\n"
                + "where {\n"
                + "	{ \n"
                + sbAuthor.toString()
                + sbDetInst.toString()
                + "	}\n"
                + "	union\n"
                + "	{\n"
                + sbDetInst0.toString()
                + sbInstitution.toString()
                + "	}\n"
                + "	union {\n"
                + sbPublication.toString()
                + "		{\n"
                + "      		select ?s2\n"
                + "			where {\n"
                + "   				?s2 <http://schema.org/affiliation> ?s1.\n"
                + sbAuthor2.toString()
                + sbDetInst.toString()
                + "			}\n"
                + "		}\n"
                + "	}\n"
                + "}\n"
                + "order by ?s ?p");
        
        System.out.println(sbQuery.toString());

        Model model = VirtModel.openDatabaseModel(graph, url, user, password);
        List<List> lstRsp = new ArrayList<List>();
        List<Map<String, Object>> listNod = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> listLks = new ArrayList<Map<String, Object>>();
        /*String sprql = "select ?s ?p ?o\n"
                + "where {{ \n"
                + "?s <http://schema.org/affiliation> ?s1.\n"
                + "?s ?p ?o.\n"
                + "{select ?s1\n"
                + "where {\n"
                + "?s1 <http://www.loc.gov/mads/rdf/v1#country> \"" + country + "\" .\n"
                + "?s1 <http://www.loc.gov/mads/rdf/v1#city> \"" + city + "\" .\n"
                + "?s1 <http://schema.org/legalName> \"" + institute + "\".\n"
                + "}}\n"
                + "}\n"
                + "union\n"
                + "{\n"
                + "?s <http://www.loc.gov/mads/rdf/v1#country> \"" + country + "\" .\n"
                + "?s <http://www.loc.gov/mads/rdf/v1#city> \"" + city + "\" .\n"
                + "?s <http://schema.org/legalName> \"" + institute + "\".\n"
                + "?s ?p ?o .\n"
                + "}\n"
                + "union {\n"
                + "?s <http://schema.org/creator> ?s2.\n"
                + "?s ?p ?o.\n"
                + "{\n"
                + "      select ?s2\n"
                + "where {\n"
                + "   ?s2 <http://schema.org/affiliation> ?s1\n"
                + "   {\n"
                + "      select ?s1\n"
                + "      where {\n"
                + "         ?s1 <http://www.loc.gov/mads/rdf/v1#country> \"" + country + "\" .\n"
                + "         ?s1 <http://www.loc.gov/mads/rdf/v1#city> \"" + city + "\" .\n"
                + "         ?s1 <http://schema.org/legalName> \"" + institute + "\".\n"
                + "      }\n"
                + "    }\n"
                + "}\n"
                + "\n"
                + "}\n"
                + "}\n"
                + "}\n"
                + "order by ?s ?p";*/
        VirtuosoQueryExecution vqm = (VirtuosoQueryExecution) VirtuosoQueryExecutionFactory.create(sbQuery.toString(), model);
        com.hp.hpl.jena.query.ResultSet results = vqm.execSelect();

        Random rdn = new Random();
        String s = "", p = "", o = "", aux = "", color = "";
        int cont = 1, group = 0;
        double size;
        color = "rgb(" + (int) (rdn.nextDouble() * 255 + 0) + "," + (int) (rdn.nextDouble() * 255 + 0) + "," + (int) (rdn.nextDouble() * 255 + 0) + ")";
        Map<String, Object> preObj = new LinkedHashMap<String, Object>();
        Map<String, Integer> mapGroup = new LinkedHashMap<String, Integer>();
        Map<String, String> mapColor = new LinkedHashMap<String, String>();
        Map<String, Double> mapSize = new LinkedHashMap<String, Double>();
        List<String> pila = new ArrayList<String>();
        while (results.hasNext()) {
            QuerySolution result = results.nextSolution();
            s = result.get("s").toString();
            p = result.get("p").toString();
            o = result.get("o").toString();

            if (!s.equals(aux)) {
                preObj = new LinkedHashMap<String, Object>();
                pila.clear();
                cont = 1;
            }

            if (p != null && !p.isEmpty()) {
                if (pila.contains(p)) {
                    p += cont;
                    cont++;
                }
                preObj.put(p, o);
                if (p.equals("http://www.w3.org/1999/02/22-rdf-syntax-ns#type")) {
                    if (mapGroup.containsKey(o)) {
                        preObj.put("group", mapGroup.get(o));
                        preObj.put("color", mapColor.get(o));
                        preObj.put("size", mapSize.get(o));
                    } else {
                        group++;
                        color = "rgb(" + (int) (rdn.nextDouble() * 255 + 0) + "," + (int) (rdn.nextDouble() * 255 + 0) + "," + (int) (rdn.nextDouble() * 255 + 0) + ")";
                        size = (rdn.nextDouble() * 10 + 1);
                        mapGroup.put(o, group);
                        mapColor.put(o, color);
                        mapSize.put(o, size);
                        preObj.put("group", group);
                        preObj.put("color", color);
                        preObj.put("size", size);
                    }
                }
                pila.add(p);
            }

            query.put(s, preObj);
            aux = s;
        }

        cont = 0;
        for (Map.Entry<String, Map<String, Object>> entry : query.entrySet()) {
            mapLink.put(entry.getKey(), cont);
            entry.getValue().put("id", String.valueOf(cont));
            entry.getValue().put("x", (rdn.nextDouble() * 1000 + 1));
            entry.getValue().put("y", (rdn.nextDouble() * 1000 + 1));
            //entry.getValue().put("size", (rdn.nextDouble() * 10 +1));
            cont++;
        }

        Map<String, Object> str;
        //Map<String, String> lks = new LinkedHashMap<String, String>();
        for (Map.Entry<String, Map<String, Object>> entry : query.entrySet()) {
            str = new LinkedHashMap<String, Object>();
            //lks = new LinkedHashMap<String, String>();

            str = replacePredicate(entry.getValue(), pathEQ);
            listNod.add(str);
            //listLks.add(lks);
        }
        listLks = getLinks();

        lstRsp.add(listNod);
        lstRsp.add(listLks);

        Pattern pat = new Pattern();
        pat.setNodes(listNod);
        pat.setLinks(listLks);

        return pat;
    }
}
