/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.model;

/**
 *
 * @author jonathan
 */
public class Institution extends Basic{
    private String country;
    private String city;

    public Institution() {
        
    }
    
    public Institution(String country, String city, String name, int group) {
        this.country = country;
        this.city = city;
        this.setName(name);
        this.setGroup(group);
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
    
}
