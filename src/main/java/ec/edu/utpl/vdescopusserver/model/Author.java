/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.model;

/**
 *
 * @author jonathan
 */
public class Author extends Basic{
    private String givenName;
    private String surName;
    private String initials;

    public Author(String givenName, String surName, String initials, String name, int group) {
        this.givenName = givenName;
        this.surName = surName;
        this.initials = initials;
        this.setName(name);
        this.setGroup(group);
    }
    
    public String getGivenName() {
        return givenName;
    }

    public void setGivenName(String givenName) {
        this.givenName = givenName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String SurName) {
        this.surName = SurName;
    }

    public String getInitials() {
        return initials;
    }

    public void setInitials(String initials) {
        this.initials = initials;
    }
    
}
