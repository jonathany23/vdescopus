/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jonathan
 */
public class ForceDirected<T> {
    private List<T> nodes = new ArrayList<T>();
    private List<Links> links = new ArrayList<Links>();

    public List<T> getNodes() {
        return nodes;
    }

    public void setNodes(T nodes) {
        this.nodes.add(nodes);
    }

    public List<Links> getLinks() {
        return links;
    }

    public void setLinks(List<Links> links) {
        this.links = links;
    }
    
}
