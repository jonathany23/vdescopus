/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdescopusserver.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author jonathan
 */
public class Pattern {
    private List<Map<String, Object>> nodes = new ArrayList<Map<String, Object>>();
    private List<Map<String, Object>> links = new ArrayList<Map<String, Object>>();

    public List<Map<String, Object>> getNodes() {
        return nodes;
    }

    public void setNodes(List<Map<String, Object>> nodes) {
        this.nodes = nodes;
    }

    public List<Map<String, Object>> getLinks() {
        return links;
    }

    public void setLinks(List<Map<String, Object>> links) {
        this.links = links;
    }

    
}
