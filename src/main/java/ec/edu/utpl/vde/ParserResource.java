/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vde;

import ec.edu.utpl.util.EquivalentNames;
import ec.edu.utpl.vde.client.GetSource;
import ec.edu.utpl.vdeparser.parser.Parser;
import java.io.File;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MultivaluedMap;

/**
 * REST Web Service
 *
 * @author jonathan
 */
@Path("parser")
public class ParserResource<T> {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of ParserResource
     */
    public ParserResource() {
    }

    /**
     * Retrieves representation of an instance of ec.edu.utpl.vde.ParserResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json")
    public String getJson() {
        //TODO return proper representation object
        throw new UnsupportedOperationException();
    }

    @GET
    @Path("names")
    @Produces("application/json"+ ";charset=utf-8")
    public List<String> getParamNames() throws Exception {
        //Extraigo los query parameters de la ruta enviada desde el Parser
        System.out.println("#####################################");
        EquivalentNames en = new EquivalentNames();
        return en.getNames("1");
        //return null;
    }
    
    @GET
    @Path("params")
    @Produces("application/json"+ ";charset=utf-8")
    public T getPubParam(@Context UriInfo info) throws Exception {
        //Extraigo los query parameters de la ruta enviada desde el Parser
        MultivaluedMap<String, String> mapParam = info.getQueryParameters();
        String json = "";
        GetSource getSource = new GetSource();
        json = getSource.getQueryJson(mapParam);
        System.out.println("#####################################");
        //System.out.println(json);
        Parser p3 = new Parser();
        System.out.println("@#@#@#@: "+mapParam.get("graph").get(0));
        return (T) p3.parse(json, mapParam.get("graph").get(0));
        //return null;
    }
    
    @GET
    @Path("{country}/{city}/{institute}/{graph}")
    @Produces("application/json"+ ";charset=utf-8")
    public T getPub4Inst(@PathParam("country") String country, @PathParam("city") String city, @PathParam("institute") String institute, @PathParam("graph") String graph) {
        try {
            String json="";
            GetSource getSource = new GetSource();
            json = getSource.getPub4Inst(country, city, institute);
            System.out.println("#####################################");
            System.out.println(json);
            Parser p3 = new Parser();
            return (T) p3.parse(json, graph);
        } catch (Exception ex) {
            Logger.getLogger(ParserResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @GET
    @Path("countries/{graph}")
    @Produces("application/json"+ ";charset=utf-8")
    public T getContries(@PathParam("graph") String graph) {
        try {
            String json="";
            GetSource getSource = new GetSource();
            json = getSource.getCountries();
            System.out.println("#####################################");
            System.out.println(json);
            Parser p3 = new Parser();
            return (T) p3.parse(json, graph);
        } catch (Exception ex) {
            Logger.getLogger(ParserResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     * PUT method for updating or creating an instance of ParserResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }

    private File File(String tmpdata) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
