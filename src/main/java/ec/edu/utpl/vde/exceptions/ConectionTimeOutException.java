/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vde.exceptions;

import com.sun.jersey.api.Responses;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.ws.soap.AddressingFeature;

/**
 *
 * @author jonathan
 */
public class ConectionTimeOutException extends WebApplicationException {

    public ConectionTimeOutException() {
        super(Responses.notFound().build());
    }

    public ConectionTimeOutException(String message) {
        super(Response.status(Responses.NOT_FOUND).entity(message).type(MediaType.APPLICATION_JSON).build());
    }
    
    
}
