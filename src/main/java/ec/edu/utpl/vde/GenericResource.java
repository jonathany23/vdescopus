/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vde;

import com.hp.hpl.jena.sparql.lib.org.json.JSONException;
import ec.edu.utpl.vde.exceptions.ConectionTimeOutException;
import ec.edu.utpl.vdescopusserver.dao.CountrysDao;
import ec.edu.utpl.vdescopusserver.dao.Source;
import ec.edu.utpl.vdescopusserver.dao.d3.Map4Tool;
import ec.edu.utpl.vdescopusserver.model.Pattern;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

/**
 * REST Web Service
 *
 * @author jonathan
 */
@Path("vde")
public class GenericResource {

    @Context
    private UriInfo context;

    /**
     * Creates a new instance of GenericResource
     */
    public GenericResource() {
    }

    @GET
    @Path("params")
    @Produces("application/json"+ ";charset=utf-8")
    public Pattern getQueryJson(@Context UriInfo info) {
        //Extraigo los query parameters de la ruta enviada desde el Parser
        MultivaluedMap<String, String> mapParam = info.getQueryParameters();
        //Si no hay parametros no hace nada
        if (mapParam!=null && !mapParam.isEmpty()) {
            Map<String, List<String>> mapParamteters = mapParam;
            Map<String, List<String>> mapParamGraph = new HashMap<String, List<String>>();
            Map<String, List<String>> mapParamVir = new HashMap<String, List<String>>();
            List<String> listVal = mapParam.get("graph");
            if (listVal!=null && !listVal.isEmpty()) {
                //Extraigo el grafo del cual se requiere mapear 
                //el nombre del parametro con los predicados de Virtuoso
                String graph = listVal.get(0);
                System.out.println("##### "+graph);
                Map4Tool m4t = new Map4Tool();
                if (graph.equals("1")) {
                    try {
                        Source src = new Source();
                        mapParamGraph = m4t.d3Miserables(mapParamteters);
                        if (mapParamGraph != null && !mapParamGraph.isEmpty()) {
                            mapParamVir = m4t.dataVir(mapParamGraph);
                            return src.queryParam(mapParamVir);
                        }
                    } catch (Exception e) {
                        System.out.println("ERROR: "+e.getCause());
                    }
                }
            } else
                System.out.println("No se ha enviado parametro Grafo");
        } else
            System.out.println("No se encontraron pametros");
        return null;
    }

    /*
     * Retrieves representation of an instance of ec.edu.utpl.vdescopusserver.GenericResource
     * @return an instance of java.lang.String
     */
    @GET
    @Produces("application/json"+ ";charset=utf-8")
    public List getJson() {
        try {
            //TODO return proper representation object
            CountrysDao cd = new CountrysDao();
            return cd.getCountries();
//            Source sr = new Source();
//            return sr.queryM();
        } catch (Exception ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
            throw new ConectionTimeOutException("Error Conectarse a Virtuoso, Por favor revise su configuracion e intente nuevamente.");
        }
        //return null;
    }
    
    @GET
    @Path("{country}")
    @Produces("application/json"+ ";charset=utf-8")
    public List<String> getCities(@PathParam("country") String country) {
        try {
            CountrysDao cd = new CountrysDao();
            return cd.getCities(country);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @GET
    @Path("{country}/{city}")
    @Produces("application/json"+ ";charset=utf-8")
    public List<String> getInstitute(@PathParam("country") String country, @PathParam("city") String city) {
        try {
            CountrysDao cd = new CountrysDao();
            return cd.getInstitutes(country,city);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @GET
    @Path("{country}/{city}/{institute}")
    @Produces("application/json"+ ";charset=utf-8")
    public Pattern getPub4Inst(@PathParam("country") String country, @PathParam("city") String city, @PathParam("institute") String institute) {
        try {
            System.out.println(institute);
    //        try {
    //            Exec ex = new Exec();
    //            return ex.execute(country, city, institute);
                 Source sr = new Source();
                return sr.queryM(country, city, institute);
    //        } catch (SQLException ex1) {
    //            Logger.getLogger(Publications4Institute.class.getName()).log(Level.SEVERE, null, ex1);
    //        return null;
    //        return null;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JSONException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @GET
    @Path("countries")
    @Produces("application/json"+ ";charset=utf-8")
    public Pattern getPub4Country(){
        try {
            Source src = new Source();
            return src.queryCountries();
        } catch (Exception e) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }
    
    @POST
    @Path("{country}")
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    public List<String> getCitiesPost(@PathParam("country") String country) {
        System.out.println("1: "+country);
        try {
            CountrysDao cd = new CountrysDao();
            return cd.getCities(country);
        } catch (SQLException ex) {
            Logger.getLogger(GenericResource.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    @DELETE
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response remove(@PathParam("id") String id) {
        return null;
    }

    /**
     * PUT method for updating or creating an instance of GenericResource
     * @param content representation for the resource
     * @return an HTTP response with content of the updated or created resource.
     */
    @PUT
    @Consumes("application/json")
    public void putJson(String content) {
    }
}
