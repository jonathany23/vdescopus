/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.util;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author jonathan
 */
public class EquivalentNames {
    
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    
    public List<String> getNames(String graph) throws Exception{
        URL path = null;
        if (graph.equals("1")) {
            path = classLoader.getResource("../json/equivalenciasD3.json");
        } else if (graph.equals("sigma")){
            path = classLoader.getResource("../json/equivalenciasSigma.json");
        }
        
        if (path!=null) {
            InputStream is = new FileInputStream(path.getPath());
            String json = getStringFromInputStream(is);
            JSONObject jsonObject = new JSONObject(json);
            JSONArray arr = jsonObject.names();
            String val="";

            List<String> listNames = new ArrayList<String>();
            
            for (int i = 0; i < arr.length(); i++) {
                val = jsonObject.get(arr.getString(i)).toString().replace("[", "").replace("]", "").replace("\"", "").trim();
                if (val != null && !val.isEmpty()) {
                    System.out.println("#####: :("+val);
                    listNames.add(val);
                } else {
                    System.out.println("#/////: :|"+arr.getString(i));
                    listNames.add(arr.getString(i));
                }
            }
            return listNames;
        }
        return null;
    }
    
    private static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
     }
}
