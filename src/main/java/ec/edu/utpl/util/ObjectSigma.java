/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Jonathan
 */
public class ObjectSigma<T> {
    private List<Map<Object, Object>> nodes = new ArrayList<Map<Object, Object>>();
    private List<T> edges = new ArrayList<T>();

    public List<Map<Object, Object>> getNodes() {
        return nodes;
    }

    public void setNodes(List<Map<Object, Object>> nodes) {
        this.nodes = nodes;
    }

    public List<T> getEdges() {
        return edges;
    }

    public void setEdges(List<T> edges) {
        this.edges = edges;
    }
    
}
