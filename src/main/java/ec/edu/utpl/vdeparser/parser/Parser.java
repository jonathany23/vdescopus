/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.edu.utpl.vdeparser.parser;

import ec.edu.utpl.util.ObjectJson;
import ec.edu.utpl.util.ObjectSigma;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/**
 *
 * @author Jonathan
 */
public class Parser<T> {
    
    ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
    
    public T parse(String json, String graph) throws Exception{
        ObjectJson objJS = new ObjectJson();
        ObjectSigma objSigma = new ObjectSigma();
        ObjectMapper objMapper = new ObjectMapper();
        ObjectJson objJson = objMapper.readValue(json, ObjectJson.class);
        List<Map<Object, Object>> nodes = new ArrayList<Map<Object, Object>>();
        List<Map<Object, Object>> edges = new ArrayList<Map<Object, Object>>();
        URL path=null;
        if (graph.equals("1"))
            //path = "C:\\Users\\Jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEParser\\src\\main\\java\\ec\\edu\\utpl\\vdeparser\\d3\\equivalenciasD3.json";
            path = classLoader.getResource("../json/equivalencias.json");
        else if (graph.equals("sigma"))
            path = classLoader.getResource("../json/equivalenciasSigma.json");
        else if (graph.equals("3"))
            //path = "C:\\Users\\Jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEParser\\src\\main\\java\\ec\\edu\\utpl\\vdeparser\\d3\\equivalenciasD3.json";
            path = classLoader.getResource("../json/equivalencias.json");
        else if (graph.equals("4"))
            //path = "C:\\Users\\jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEScopus\\src\\main\\java\\ec\\edu\\utpl\\vdeparser\\parser\\equivalenciasReddit.json";
            path = classLoader.getResource("../json/equivalenciasReddit.json");
        else if (graph.equals("5"))
            path = classLoader.getResource("../json/equivalencias.json");
        
        if (path != null) {
            for (Object map : objJson.getNodes()) {
                nodes.add(replacePredicate(map,path));
            }
        } else 
            System.out.println("Codigo de Grafica no definido o Incorrecto");
        
        System.out.println("List: "+nodes.size());
        if (graph.equals("1") || graph.equals("5")) {
            objJS.setNodes(nodes);
            objJS.setLinks(objJson.getLinks());
            return (T) objJS;
        } else if (graph.equals("sigma")) {
            objSigma.setNodes(nodes);
            for (Object map : objJson.getLinks()) {
                edges.add(parseLinks(map));
            }
            objSigma.setEdges(edges);
            return (T) objSigma;
        } else if (graph.equals("3")){
            System.out.println("############################ 3333333333");
            List<Map<Object, Object>> listLinks = new ArrayList<Map<Object, Object>>();
            for (Object map : objJson.getLinks()) {
                listLinks.add(parseLinks(map));
            }
            
            int target=0, source, cont=0;
            //List<Map<Object, Object>> litChild = new ArrayList<Map<Object, Object>>();
            Map<Object, Object> mapChild = new HashMap<Object, Object>();
            Map<Integer, Map<Object, Object>> mapAux = new HashMap<Integer, Map<Object, Object>>();
            Map<Integer, Map<Object, Object>> mapAux2 = new HashMap<Integer, Map<Object, Object>>();
            List<Map<Object, Object>> listMap;// = new ArrayList<Map<Object, Object>>();
            for (Map<Object, Object> map : listLinks) {
                if (map.get("target")!= null && map.get("source")!=null) {
                    target= Integer.parseInt(map.get("target").toString());
                    source = Integer.parseInt(map.get("source").toString());
                    
                    if (!mapAux.containsKey(source))
                        mapAux.put(source, nodes.get(source));
                    if (!mapAux.containsKey(target))
                        mapAux.put(target, nodes.get(target));
                }                 
//                    //if (mapChild.isEmpty()) {
//                        //mapChild = nodes.get(idxNodo);
//                    //}
//                    //mapChild.put("children", nodes.get(source));
////                    litChild.add(mapChild);
            }
            
            for (int i = listLinks.size(); i > 0; i--) {
                //source = Integer.parseInt(listLinks.get(i-1).get("source").toString());
                target = Integer.parseInt(listLinks.get(i-1).get("target").toString());
                //mapAux.get(target).put("children", mapAux.get(source));
                listMap = new ArrayList<Map<Object, Object>>();
                for (int j = listLinks.size(); j > 0; j--) {
                    if (target == Integer.parseInt(listLinks.get(j-1).get("target").toString())) {
                        source = Integer.parseInt(listLinks.get(j-1).get("source").toString());
                        listMap.add(mapAux.get(source));
                    }
                }
                mapAux.get(target).put("children", listMap);
                mapAux2.put(target, mapAux.get(target));
            }
            System.out.println("FIN ############################ 3333333333");
//            return (T) litChild;
            List<Map<Object, Object>> listF = new ArrayList<Map<Object, Object>>();
            listF.add(mapAux2.get(target));
            return (T) listF;
        } else if (graph.equals("4")) {
            objJS.setNodes(nodes);
            
            //Map<String, Double> weight = new HashMap<String, Double>();
            Random rnd = new Random();
            //weight.put("weight", rnd.nextDouble());
            //objJson.getLinks().add(weight);
            ObjectMapper m = new ObjectMapper();
            List<Map<Object, Object>> lstMap = new ArrayList<Map<Object, Object>>();
            Map<Object, Object> as;// = m.convertValue(objLinks, Map.class);
            //as = null;
            for (Object objLinks : objJson.getLinks()) {
                as = m.convertValue(objLinks, Map.class);
                as.put("weight", rnd.nextDouble());
                lstMap.add(as);
            }
//            for (Object map : objJson.getLinks()) {
//                System.out.println("sds: "+map);
//            }
            //objJS.setLinks(objJson.getLinks());
            objJS.setLinks(lstMap);
            return (T) objJS;
        }
        return null;
    }
    
    private Map<Object, Object> replacePredicate(Object value, URL path) throws FileNotFoundException, JSONException {
        //String path = "C:\\Users\\Jonathan\\Dropbox\\UTPL\\materias\\Tesis\\App\\VDEParser\\src\\main\\java\\ec\\edu\\utpl\\vdeparser\\d3\\equivalenciasD3.json";
        InputStream is = new FileInputStream(path.getPath());
        String json = getStringFromInputStream(is);
        JSONObject jsonObject = new JSONObject(json);
        JSONArray arr = jsonObject.names();
        String val;
        boolean isNames = false;
        
        ObjectMapper m = new ObjectMapper();
        Map<Object, Object> as = m.convertValue(value, Map.class);
        
        Map<Object, Object> predicates = new HashMap<Object, Object>();
        for (Map.Entry<Object, Object> entry : as.entrySet()) {
            for (int i = 0; i < arr.length(); i++) {
                if (entry.getKey().equals(arr.getString(i))) {
                    isNames = true;
                    val = jsonObject.get(arr.getString(i)).toString().replaceAll("\\[", "").replaceAll("\\]", "").replaceAll("\\\"", "");
                    if (!val.isEmpty()) {
                        predicates.put(val, entry.getValue());
                    } else {
                        predicates.put(entry.getKey(), entry.getValue());
                    }
                    break;
                }
            }
            if (!isNames) {
                predicates.put(entry.getKey(), entry.getValue());
            }
            isNames = false;
        }
        
        return predicates;
    }
    
    private static String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
     }

    private Map<Object, Object> parseLinks(Object map) {
        ObjectMapper m = new ObjectMapper();
        Map<Object, Object> as = m.convertValue(map, Map.class);
        
        for (Map.Entry<Object, Object> entry : as.entrySet()) {
            if (entry.getKey().equals("source")) {
                entry.setValue(entry.getValue().toString());
            } else if (entry.getKey().equals("target")) {
                entry.setValue(entry.getValue().toString());
            }
        }
        return as;
    }
}
