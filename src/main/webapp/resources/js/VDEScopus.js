/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

var protocol = window.location.protocol;
var host = window.location.host;
var pathArray = window.location.pathname.split( '/' );
var pathname = "/"+pathArray[1]+"/";

function loadFieldsNames(id) {
    $.ajax({
        type: "GET",
        url: protocol+"//"+host+pathname+"webresources/parser/names",
        dataType: "json",
        success: function(data) {
            var c = document.getElementById(id);
            data.forEach(function(names) {
                var t = document.createElement("option");
                t.value = names;
                t.textContent = names;
                c.appendChild(t);
            });
        }
    });
}

var strCountry;
function loadCities() {
    var elem = document.getElementById('cities');
    elem.innerHTML = '';
    var op = document.createElement("option");
    op.value = "0";
    op.textContent = "-Select-"
    elem.appendChild(op);
    //elem.parentNode.removeChild(elem);

    var opInst = document.getElementById('institute');
    opInst.innerHTML = '';
    elem.appendChild(op);
    //return false;

    var e = document.getElementById("countries");
    strCountry = e.options[e.selectedIndex].value;

    var rmLi = document.getElementById('unod');
    if (rmLi!=null)
        rmLi.innerHTML = '';

    $.ajax({
        type: "GET",
        url: protocol+"//"+host+pathname+"webresources/vde/" + strCountry,
        dataType: "json",
        success: function(data) {
            var c = document.getElementById('cities');
            data.forEach(function(cities) {
                var t = document.createElement("option");
                t.value = cities;
                t.textContent = cities;
                c.appendChild(t);
            });
        },
        failure: function(errMsg) {
            console.log("ERROR: "+errMsg);
            alert("Error al obtener datos para el País: "+strCountry);
        }
    });
}

var strCity;
function loadInstitutes() {
    var opInst = document.getElementById('institute');
    opInst.innerHTML = '';
    var op = document.createElement("option");
    op.value = "0";
    op.textContent = "-Select-"
    opInst.appendChild(op);

    var e = document.getElementById("cities");
    strCity = e.options[e.selectedIndex].value;

    var rmLi = document.getElementById('unod');
    if (rmLi!=null)
        rmLi.innerHTML = '';

    $.ajax({
        type: "GET",
        url: protocol+"//"+host+pathname+"webresources/vde/" + strCountry + "/" + strCity,
        dataType: "json",
        success: function(data) {
            var i = document.getElementById('institute');
            data.forEach(function(intitute) {
                var t = document.createElement("option");
                t.value = intitute;
                t.textContent = intitute;
                i.appendChild(t);
            });
        },
        failure: function(errMsg) {
            console.log("ERROR: "+errMsg);
            alert("Error al obtener datos para la Ciudad: "+strCity);
        }
    });
}

var strInstitute;
function loadData(path) {
    var ruta = path;
    //console.log('Hola estoy en Load data....');
    var rmLi = document.getElementById('unod');
    rmLi.innerHTML = '';

    var e = document.getElementById("institute");
    strInstitute = e.options[e.selectedIndex].value;
    console.log(strInstitute);

    //Elimina etiqueta SVG previos.
    var svg = document.getElementById('svg');
    if (svg != null) {
        var aux = svg.parentNode;
        aux.removeChild(svg);
    }

    //Elimina div de popup anterior.
    var className = document.getElementsByClassName('pop');
    if (className != null) {
        console.log('className: ' + className);
        var aux = className.parentNode;
        if (aux != null) {
            console.log('aux: ' + aux);
            aux.removeChild(className);
        }
    }

    // var className = document.getElementsByClassName('desc');
    // if (className != null) {
    //     console.log('className: ' + className);
    //     var aux = className.parentNode;
    //     if (aux != null) {
    //         console.log('aux: ' + aux);
    //         aux.removeChild(className);
    //     }
    // }
    
    if (ruta==null){
        ruta = protocol+"//"+host+pathname+"webresources/parser/" + strCountry + "/" + strCity + "/" + strInstitute + "/1";
    }else{
        ruta = protocol+"//"+host+pathname+"webresources/parser/params"+path;
    }
    
    console.log("ruta: "+ruta);

    $.ajax({
        type: "GET",
        url: ruta,
        dataType: "json",
        success: function(data) {
            var width = 1030, height = 600;

            var color = d3.scale.category20();

            var force = d3.layout.force()
                    .charge(-120)
                    .linkDistance(30)
                    .size([width, height]);

            //var svg = d3.select("body").append("svg")
            var svg = d3.select(".center").append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    .attr("id", 'svg');

            // var desc = d3.select("body").append("div")
            //         .attr("width", 150)
            //         .attr("height", height);
            //         // .attr("class", "desc");

            var pop = d3.select("body").append("div")
                    .attr("width", 300)
                    .attr("height", height)
                    .attr("class", "pop");

//d3.json("miserables.json", function(error, graph) {
            graph = data;
            console.log(graph);
            force
                    .nodes(graph.nodes)
                    .links(graph.links)
                    .start();

            var link = svg.selectAll(".link")
                    .data(graph.links)
                    .enter().append("line")
                    .attr("class", "link")
                    .style("stroke-width", function(d) {
                return Math.sqrt(d.value);
            });
            console.log(force.drag);

            var node = svg.selectAll(".node")
                    .data(graph.nodes)
                    .enter().append("circle")
                    .attr("class", "node")
                    .attr("r", 5)
                    .style("fill", function(d) {
                return color(d.group);
            })
                    .call(force.drag);


//                var info = desc.selectAll(".desc")
//                    .data(graph.nodes)
//                    .enter()
//                     .append("div")
//                     .attr("class","sd")
//                    .text(function(d) { 
//                            return d.name + " "+ d.group+" "
//                        })
//                    .append("div")
//                    .attr("class","cir")
//                    .style("background", function(d) { return color(d.group); });

            $(document).ready(function() {
                // $(".desc").hide();

                var cc = 0;
                graph.nodes.forEach(function(nod) {
                    if (cc % 2 == 0) {
                        d3.select(".lnod").select("#unod").append("li").attr("class", "pnod").text(nod.name);
                    } else {
                        d3.select(".lnod").select("#unod").append("li").attr("class", "inod").text(nod.name);
                    }
                    cc++;
                });
                var nam = "";
                $(".pnod, .inod").click(function() {
                    var tag = $(this).text();
                    node[0].forEach(function(nod) {
                        nam = $(nod).text();
                        // console.log(find+" # "+nam);
                        if (tag == nam) {
                            // console.log("Hola")
                            $(nod).attr("r", 10);
                        } else {
                            $(nod).attr("r", 5);
                        }
                        co++;
                    });
                })

                cc = 0;
                var arrNames = new Array();
                graph.nodes.forEach(function(nod) {
                    arrNames[cc] = nod.name;
                    cc++;
                });

                $("#search").autocomplete({
                    source: arrNames
                });

                var find = "";
                var co = 0;

                $("#search").keydown(function() {
                    find = $("#search").val();
                    // console.log(node[0][0].);
                    node[0].forEach(function(nod) {
                        nam = $(nod).text();
                        // console.log(find+" # "+nam);
                        if (find == nam) {
                            // console.log("Hola")
                            $(nod).attr("r", 10);
                        } else {
                            $(nod).attr("r", 5);
                        }
                        co++;
                    });
                });


                $(".node").click(function() {
                    $(".tag").remove();

//            $(this).mouseover(function (){
//                $(this).attr("r",8);
//            });

                    //$(".desc").show();
                    $("#sNav_rigth").show();
                    var name = $(this).text();
                    // var col = $(this).css("fill");
                    // var index = "";
                    // var group = "";
                    // var city = "";
                    // var country = "";
                    // var givenName = "";
                    // var surName = "";
                    // var initials = "";
                    // var date = "";
                    // var section = "";
                    // var volume = "";
                    // var keyWords = "";
                    // var description = "";

                    graph.nodes.forEach(function(nod) {
                        if (name == nod.name) {
                            // index = nod.index;
                            // group = nod.group;
                            // city = nod.city;
                            // country = nod.country;
                            // givenName = nod.givenName;
                            // surName = nod.surName;
                            // initials = nod.initials;
                            // date = nod.date;
                            // section = nod.section;
                            // volume = nod.volume;
                            // keyWords = nod.keyWords;
                            // description = nod.description;
                            console.log("data: "+JSON.stringify(nod));
                            click(nod);
                        }
                    });

                    // d3.select(".desc").append("p").attr("class", "tag").text("Nombre: " + name);
                    // d3.select(".desc").append("p").attr("class", "tag").text("Grupo: " + group);
                    // d3.select(".desc").append("p").attr("class", "tag").text("Color: ").append("div").attr("class", "cir").style("background", col);
                    // if (city != null)
                    //     d3.select(".desc").append("p").attr("class", "tag").text("Ciudad: " + city);
                    // if (country != null)
                    //     d3.select(".desc").append("p").attr("class", "tag").text("País: " + country);
                    // if (givenName != null)
                    //     d3.select(".desc").append("p").attr("class", "tag").text("Nombre: " + givenName);
                    // if (surName != null)
                    //     d3.select(".desc").append("p").attr("class", "tag").text("Apellido: " + surName);
                    // if (initials != null)
                    //     d3.select(".desc").append("p").attr("class", "tag").text("Inicilaes: " + initials);
                    // if (date != null)
                    //     d3.select(".desc").append("p").attr("class", "tag").text("Fecha: " + date);
                    // if (section != null)
                    //     d3.select(".desc").append("p").attr("class", "tag").text("Sección: " + section);
                    // if (volume != null)
                    //     d3.select(".desc").append("p").attr("class", "tag").text("Volumen: " + volume);
                    // if (keyWords != null)
                    //     d3.select(".desc").append("p").attr("class", "tag").text("Pal. Clave: " + keyWords);
                    // if (description != null)
                    //     d3.select(".desc").append("p").attr("class", "tag").text("Descripción: " + description);
                })

                var arrNamesS = new Array();
                var arrNamesT = new Array();

                $(function() {
                    var moveLeft = 20;
                    var moveDown = 10;

                    $(".node").hover(function(e) {
                        if ($(this).attr("class") == "node") {
                            $('.pop').show()
                                    .css('top', e.pageY + moveDown)
                                    .css('left', e.pageX + moveLeft)
                                    .appendTo('body');

                            var name = $(this).text();
                            //var name = document.getElementsByClassName('.node')[this].innerText
                            var index = "";

                            var contS = 0, contT = 0;

                            graph.nodes.forEach(function(nod) {
                                if (name == nod.name) {
                                    index = nod.index;
                                }
                            });

                            var cf = "";
                            var arrChildColors = new Array();
                            graph.links.forEach(function(lk) {
                                if (lk.source.index == index) {
                                    var nameP = "";
                                    var groupP = 0;

                                    // nameP = graph.links[index].target.name;
                                    // groupP = graph.links[index].target.group
                                    nameP = lk.target.name;
                                    groupP = lk.target.group;
                                    $('.node').each(function() {
                                        if ($(this).text() == nameP) {
                                            cf = $(this).css("fill");
                                        }
                                    });

                                    arrNamesS[contS] = nameP + ", " + groupP + ", ";
                                    contS++;
                                } else if (lk.target.index == index) {
                                    var nameC = lk.source.name;
                                    var groupC = lk.source.group;

                                    arrNamesT[contT] = nameC + ", " + groupC + ", ";

                                    $('.node').each(function() {
                                        if ($(this).text() == nameC) {
                                            arrChildColors[contT] = $(this).css("fill");
                                        }
                                    });
                                    contT++;
                                }
                            });

                            pop.append("p").attr("class", "title").text("Nombre: " + name);
                            pop.append("p").attr("class", "title").text("Nodo Padre:");
                            arrNamesS.forEach(function(arr) {
                                pop.append("p").attr("class", "parent").text(arr).append("div").attr("class", "cir").style("background", cf);
                                //pop.append("p").attr("class","parent").text(arr);
                                ////d3.select(".desc").append("p").attr("class","tag").text("Color: ").append("div").attr("class","cir").style("background", col);
                            });

                            if (arrNamesT.length != 0) {
                                pop.append("p").attr("class", "title").text("Nodos Hijos:");
                                var cc = 0;
                                arrNamesT.forEach(function(arr) {
                                    pop.append("p").attr("class", "child").text(arr).append("div").attr("class", "cir").style("background", arrChildColors[cc]);
                                    cc++;
                                });
                            }
                        }
                    }, function() {
                        $('.pop').hide();
                        arrNamesS = [];
                        arrNamesT = [];
                        $(".parent").remove();
                        $(".child").remove();
                        $(".title").remove();
                    });

                    $(".node").mousemove(function(e) {
                        $(".pop").css('top', e.pageY + moveDown).css('left', e.pageX + moveLeft);
                    });
                });

            });

            node.append("title").text(function(d) {
                return d.name;
            });

            force.on("tick", function() {
                link.attr("x1", function(d) {
                    return d.source.x;
                })
                        .attr("y1", function(d) {
                    return d.source.y;
                })
                        .attr("x2", function(d) {
                    return d.target.x;
                })
                        .attr("y2", function(d) {
                    return d.target.y;
                });

                node.attr("cx", function(d) {
                    return d.x;
                })
                        .attr("cy", function(d) {
                    return d.y;
                });
            });
            //});
        },
        failure: function(errMsg) {
            console.log("ERROR: "+errMsg);
            alert("Error al obtener datos para la Institución: "+strInstitute);
        }
    });
}

var data;
var w = 900, h = 700, node, link, root;                    
var force;
var vis;
function loadDataNodes() {
    var ruta;
    //console.log('Hola estoy en Load data....');
//    var rmLi = document.getElementById('unod');
//    rmLi.innerHTML = '';

    var e = document.getElementById("institute");
    strInstitute = e.options[e.selectedIndex].value;
    console.log(strInstitute);

    //Elimina etiqueta SVG previos.
    var svg = document.getElementById('svg');
    if (svg != null) {
        var aux = svg.parentNode;
        aux.removeChild(svg);
    }

    //Elimina div de popup anterior.
    var className = document.getElementsByClassName('pop');
    if (className != null) {
        console.log('className: ' + className);
        var aux = className.parentNode;
        if (aux != null) {
            console.log('aux: ' + aux);
            aux.removeChild(className);
        }
    }

    // var className = document.getElementsByClassName('desc');
    // if (className != null) {
    //     console.log('className: ' + className);
    //     var aux = className.parentNode;
    //     if (aux != null) {
    //         console.log('aux: ' + aux);
    //         aux.removeChild(className);
    //     }
    // }
    
    if (ruta==null){
        ruta = protocol+"//"+host+pathname+"webresources/parser/" + strCountry + "/" + strCity + "/" + strInstitute + "/3";
    }else{
        ruta = protocol+"//"+host+pathname+"webresources/parser/params"+path;
    }
    
    console.log("ruta: "+ruta);

    $.ajax({
        type: "GET",
        url: ruta,
        dataType: "json",
        success: function(datos) {
            //var data = JSON.stringify(datos);
           data = datos

            force = d3.layout.force()
                    .charge(-2000) // *** establecido en -200, al modificarle se acoplan mejor los elementos
//            .friction(-.2)
//            .gravity(0.4)
                    .size([w, h]);

            vis = d3.select("#chart").append("svg")
                    .attr("width", w)
                    .attr("height", h)
                    .attr("id", 'svg');
            
            var dataJson = JSON.stringify(data);
            var rootTemp = JSON.parse(dataJson);
            delete rootTemp[0].children;
            //console.log(JSON.stringify(data[0]));
            root = rootTemp[0];

            update();
        }
    });
}

$(document).ready(function() {

    $.ajax({
        type: "GET",
        url: protocol+"//"+host+pathname+"webresources/vde",
        dataType: "json",
        success: function(data) {
            // var c = document.getElementById(id);
            // data.forEach(function(names) {
            //     var t = document.createElement("option");
            //     t.value = names;
            //     t.textContent = names;
            //     c.appendChild(t);
            loadFieldsNames('cmb_find');
                //var countries = #{newJerseyClient.json};
                var s = document.getElementById('countries');

                data.forEach(function(country) {
                    var t = document.createElement("option");
                    t.value = country;
                    t.textContent = country;
                    s.appendChild(t);
                }); 
                $(".input_fields_wrap").hide();
            //});
        },
        error: function(e){
            console.log("ERROR: "+e);
        },
        dataType: "json",
        contentType: "application/json"
    });


    var max_fields = 10; //maximum input boxes allowed
    var wrapper = $(".input_fields_wrap_add"); //Fields wrapper
    var add_button = $(".add_field_button"); //Add button ID

    var x = 1; //initlal text box count
    $(add_button).click(function(e) { //on add input button click
        e.preventDefault();
        if (x < max_fields) { //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="fields-wrap"><select name="find'+x+'" size="1" id="cmb_find'+x+'" class="cmb_find"></select><input type="text" name="find'+x+'" class="input-find" id="input_find'+x+'"/><a href="#" class="remove_field">Borrar</a></div>'); //add input box
            loadFieldsNames("cmb_find"+x);
        }
    });

    $(wrapper).on("click", ".remove_field", function(e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    })

    //########################################
    var chage = false;
    $("#sh_search").click(function() {
        if (chage==false){
          $(".input_fields_wrap").show(1000);  
          chage=true;
        } else{
            $(".input_fields_wrap").hide(1000);  
            chage=false;
        }
    });
    
    $(".searchServer").click(function(){
        var path;
        var p;
        var so;
        //var country, city, institute;
        
        path="?graph=1&country="+strCountry+"&city="+strCity+"&name="+strInstitute;
        $(".input-find").each(function() {
            so = $(this).val();
            if (so!=null && so.length!=0){
                path+="&";
                var c = document.getElementsByName($(this).attr("name"));
                p = $(c).val();
                path+=p+"="+$(this).val();
            }
        });
        console.log(path);
        loadData(path);
    });
});

//##################################3 NODES

var svg2 = d3.select("body").append("svg").append("defs");
            
            function agregarFila(item) {
                var tbody = document.getElementById('detalle').getElementsByTagName("tbody")[0];
                var row = document.createElement("tr");

                var td = document.createElement("td");
                row.appendChild(td).innerHTML = item.label;

                var td = document.createElement("td");
                if(item.label == 'Url' && item.valor != '')
                    row.appendChild(td).innerHTML =  '<a href="'+item.valor +'" target="_blank"> <img  class="img-rounded"  width="20%;" src="'+ item.valor +'" > </a>';
                else
                    row.appendChild(td).innerHTML = item.valor;

                tbody.appendChild(row);
            }

            function deleteRow(tableID) {
                try {
                    var tbody = document.getElementById(tableID).getElementsByTagName("tbody")[0];
                    var rowCount = tbody.rows.length;
                    //alert('a eliminar: '+rowCount);
                    for (var i = 0; i < rowCount; ) {
                        tbody.deleteRow(i);
                        rowCount--;
                    }
                } catch (e) {
                    alert(e);
                }
            }
            
function update() {
//    vis.append("defs").selectAll("marker")
//        .data(["suit", "licensing", "resolved"])
//  .enter().append("marker")
//    .attr("id", function(d) { return d; })
//    .attr("viewBox", "0 -5 10 10")
//    .attr("refX", 15)
//    .attr("refY", -1.5) 
//    .attr("markerWidth", 6)
//    .attr("markerHeight", 6)
//    .attr("orient", "auto")
//  .append("path")
//    .attr("d", "M0,-5L10,0L0,5");
//    
//   var path = svg.append("g").selectAll("path")
//    .data(force.links())
//  .enter().append("path")
//    .attr("class", function(d) { return "link " + d.type; })
//    .attr("marker-end", function(d) { return "url(#" + d.type + ")"; });
    
    
    var duration = d3.event && d3.event.altKey ? 500 : 500;
    
    var nodes = flatten(root);
    nodes.reverse();
    nodes = nodes.sort(function (a, b) {
        return a.index - b.index;
    });

    var links = d3.layout.tree().links(nodes);
    
    link = vis.selectAll(".link")
        .data(links); 
    link.enter().append("line")
        .attr("class", "link")
        .on("click", function(d){
                        //alert('click en linea de nodo: '+d);
        });
        
    link.append("text")
//    .attr("dx", function(d){
//         return d.url != '' ?  50 : -25;
//    })
//    .attr("dy", "0.35em")
//    .style("font-size", "12px")
    .text(function (d) {
        return d.name
    });    
        

    link.exit().remove();
    
//    console.log(nodes);

    // Restart the force layout.
    force.nodes(nodes)
        .links(links)
        .linkDistance(120)
        .start();



    

    var node = vis.selectAll("g.node")
        .data(nodes);


    var groups = node.enter().append("g") //g
        .attr("class", "node")
        .attr("id", function (d) {
    console.log("dddd: "+JSON.stringify(d));
        return d.id
    })
        .on('click', click)
        .call(force.drag);

    /*tipos de objetos consulatado de: https://leanpub.com/D3-Tips-and-Tricks/read
    circle
    rect
    polyline
    */
    groups.append("circle") //image
//        .attr("xlink:href", "https://github.com/favicon.ico")
        .attr("r", 40)
//        .attr("r", function (d){ 
//            return (d.value/1.5);
//        })
        .attr("x", "-75px")
        .attr("y", "0px")
        .attr("width", "100%")
        .attr("height", "100%")

        .attr("cx", -4) //-4
        .attr("cy", -4) //-4
        .attr("xlink:href", function(d) {
                svg2.append("pattern")
                .attr("patternUnits","userSpaceOnUse")
                .attr("id",'img_'+d.id)
                .attr("x", "-75px")
                .attr("y", "-80px")
                .attr("width", "100%")
                .attr("height", "100%")
                .append("image")
                .attr("x", "-75px")
                .attr("y", "0px")
                .attr("width", "100%")
                .attr("height", "100%")
                .attr("xlink:href",d.url);
        })
        .style("fill",function(d){
            //Establece color de fondo de los circulos
            //return d.url != '' ?  'url(#img_'+d.id+')' : d.type;
            return '#4285f4';
        })
        .style("border","30px solid #ccc")
        .style("stroke",function (d){ return d.children || d._children ? "blue" : "blue" })
//        .style("border","2px solid");
//        .style("border-radius","25px");
        .style("stroke-width",function (d){ return d.children || d._children ? "10px" : "8px"})
//        .style("margin","20px")
//        .style("border","100px solid #FFF")
        .style("padding","200px")
        .style("stroke-opacity",0.3);
        
//        .style("border","10px solid #FFF")
//        .style("border-radius","100px");
//        .style("box-shadow","0 0 2px #888");  
        
        
        
//        .style("border-radius;","50px")
//        .style("border","3px solid #ccc");

    groups.append("text")
        .attr("dx", function(d){
             return d.url != '' ?  50 : -25;
        })
        .attr("dy", "0.35em")
        .style("font-size", "12px")
//        .style("color", function(d){
//             return d.url != '' ?  "black" : "white";
//        })
        .attr("text-anchor", function(d) {
            return d.children || d._children ? "end" : "start";
        })
        .text(function (d) {
            return d.name
        });
    
    
//    groups.append("text")         // append text
//    .style("fill", "black")   // fill the text with the colour black
//    .attr("dy", ".35em")           // set offset y position
//    .attr("text-anchor", "middle") // set anchor y justification
//    .attr("transform", "translate(200,100) rotate(10)")
//    .text("Hello World");
    
//    groups.append("text")            // append text
//    .style("fill", "black")      // make the text black
//    .style("writing-mode", "tb") // set the writing mode
//    .attr("x", 200)         // set x position of left side of text
//    .attr("y", 100)         // set y position of bottom of text
//    .text("Hello World");   // define the text to display
    
//    groups.append("polyline")       // attach a polyline
//    .style("stroke", "black")   // colour the line
//    .style("fill", "none")      // remove any fill colour
//	.style("stroke-width", 20)  // colour the line
//	.style("stroke-linejoin", "round")  // shape the line join
//    .attr("points", "100,50, 200,150, 300,50");  // x,y points 
    
//    groups.append("text")
//        .attr("dx", function(d){
//             return -25;
//        })
//        .attr("dy", "7em")
//        .style("font-size", "10px")
//        .attr("text-anchor", function(d) {
//            return d.children || d._children ? "end" : "start";
//        })
//        .text(function (d) {
//            if(d.url != '')
//                return "***enlace***";
//        });
    
    // Transition nodes to their new position.
//    var nodeUpdate = node.transition()
//            .duration(duration)
//            .attr("transform", function(d) {
//        return "translate(" + d.y + "," + d.x + ")";
//    });
//    nodeUpdate.select("circle")
//            .attr("r", function(d) {
//        return d.value;
//    });
//    nodeUpdate.select("text")
//            .style("fill-opacity", 1);

    node.exit().remove();


    force.on("tick", function () {
        link.attr("x1", function (d) {
            return d.source.x; 
        })
            .attr("y1", function (d) {
            return d.source.y;
        })
            .attr("x2", function (d) {
            return d.target.x;
        })
            .attr("y2", function (d) {
            return d.target.y;
        });

        node.attr("transform", function (d) {
            return "translate(" + d.x + "," + d.y + ")";
        });
    });
}


// Color leaf nodes orange, and packages white or blue.
function color(d) {
    return d._children ? "#3182bd" : d.children ? "#c6dbef" : "#fd8d3c";
}

// Toggle children on click.
function click(d) {
    // console.log("clic: "+JSON.stringify(d));
    deleteRow('detalle');

    // console.log("clic2: "+Object.keys(d));
    // var detalle = new Array("id","name","parent","description","url","value");
    var detalle = Object.keys(d);
    //var label = new Array("Identificador","Nombre","Nodo padre","Descripción","Url","Valor");
    var label = Object.keys(d);
    
    for (i = 0; i < detalle.length; i++) {
        var objeto = {
            label: label[i],
            valor: d[detalle[i]]
        }
        agregarFila(objeto);
    }
                
    if (d.children) {
        d._children = d.children;
        d.children = null;
        update();
    } else if (d._children) {
        d.children = d._children;
        d._children = null;
        update();
    } else {
        //console.log("data: "+data);
        json = buscarNodo(data,d.id);
        console.log("json.children: "+JSON.stringify(json.children));
        d.children = json.children;
        update();
    }
}

// Returns a list of all nodes under the root.
function flatten(root) {
    var nodes = [],
        i = 0;

    function recurse(node) {
        if (node.children) node.children.forEach(recurse);
        if (!node.id) node.id = ++i;
        nodes.push(node);
    }

    recurse(root);
    return nodes;
}

function buscarNodo(data,id){
    //console.log("buscarNodo: "+JSON.stringify(data));
    var obj_children; 
    var childrens = new Array();
        for (i = 0; i < data.length; i++) {
            var node = data[i];
            console.log(node.id);
            console.log(node.name);
            console.log(id);
            if( node.id.localeCompare(id) == 0){
                var children = node.children;
                for (j = 0; j < children.length; j++) {
                    var ch = {
                        name:children[j].name,
                        id:children[j].id,
                        description: children[j].description,
                        value:children[j].value,
                        url:children[j].url,
                        type:children[j].type,
                        parent:children[j].parent
                    }
                    childrens.push(ch);
                }    
                obj_children = {
                    children:childrens
                }
                return obj_children;
            }else
                if(node.children)
                     return buscarNodo(node.children, id);
        } 
    }

// $(document).ready(function() {
//     var nav = document.getElementById('access_nav'),
//     body = document.body
//     pMenu = document.getElementById('menu');

//     console.log(nav.getAttribute("href"));

//     nav.addEventListener('click', function() {
//         if (nav.getAttribute("href") == "#main_nav") {
//             nav.setAttribute("href", "#body");
//             pMenu.setAttribute("class", "#menu");
//         }else{
//             nav.setAttribute("href", "#main_nav");
//             pMenu.setAttribute("class", "#menu2");
//         }
//         body.className = body.className? '' : 'with_nav';
//         e.preventDefault();
//     });
// });

$(document).ready(function() {
    var nav = document.getElementById('access_nav');

    // console.log("nav: "+nav);
    if (nav!=null) {
        var chage2 =false;
        nav.addEventListener('click', function() {
            if (chage2==false){
              $("#main_nav").show(100);  

              chage2=true;
            } else{
                $("#main_nav").hide(100);  
                chage2=false;
            }
        });
    };

    var navR = document.getElementById('nav_rigth');

    if (navR!=null) {
        var chage3 =false;
        navR.addEventListener('click', function() {
            if (chage3==false){
              $("#sNav_rigth").show(100);  

              chage3=true;
            } else{
                $("#sNav_rigth").hide(100);  
                chage3=false;
            }
        });
    };

    var itemClick = $(this).attr("id");
    // console.log('Cliccc: '+itemClick);
    if (itemClick != 'sNav_rigth') {
        $("#sNav_rigth").hide(100);  
            chage3=false;
    };
});

// var chage = false;
//     $("#sh_search").click(function() {
//         if (chage==false){
//           $(".input_fields_wrap").show(1000);  
//           chage=true;
//         } else{
//             $(".input_fields_wrap").hide(1000);  
//             chage=false;
//         }
//     }); 


function loadCountries(path) {
    var ruta = path;
    
    var rmLi = document.getElementById('unod');
    rmLi.innerHTML = '';

    //Elimina etiqueta SVG previos.
    var svg = document.getElementById('svg');
    if (svg != null) {
        var aux = svg.parentNode;
        aux.removeChild(svg);
    }

    //Elimina div de popup anterior.
    var className = document.getElementsByClassName('pop');
    if (className != null) {
        console.log('className: ' + className);
        var aux = className.parentNode;
        if (aux != null) {
            console.log('aux: ' + aux);
            aux.removeChild(className);
        }
    }

    
    if (ruta==null){
        ruta = protocol+"//"+host+pathname+"webresources/parser/countries/5";
    }
    
    console.log("ruta: "+ruta);

    $.ajax({
        type: "GET",
        url: ruta,
        dataType: "json",
        success: function(data) {
            var width = 1030, height = 600;

            var color = d3.scale.category20();

            var force = d3.layout.force()
                    .charge(-120)
                    .linkDistance(30)
                    .size([width, height]);

            //var svg = d3.select("body").append("svg")
            var svg = d3.select(".center").append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    .attr("id", 'svg');


            var pop = d3.select("body").append("div")
                    .attr("width", 300)
                    .attr("height", height)
                    .attr("class", "pop");

//d3.json("miserables.json", function(error, graph) {
            graph = data;
            console.log(graph);
            force
                    .nodes(graph.nodes)
                    .links(graph.links)
                    .start();

            var link = svg.selectAll(".link")
                    .data(graph.links)
                    .enter().append("line")
                    .attr("class", "link")
                    .style("stroke-width", function(d) {
                return Math.sqrt(d.value);
            });
            console.log(force.drag);

            var node = svg.selectAll(".node")
                    .data(graph.nodes)
                    .enter().append("circle")
                    .attr("class", "node")
                    //.attr("r", 5)
                    .attr("r", function(d){
                        return d.r;
                    })
                    .style("fill", function(d) {
                return color(d.group);
            })
                    .call(force.drag);


            $(document).ready(function() {

                var cc = 0;
                graph.nodes.forEach(function(nod) {
                    if (cc % 2 == 0) {
                        d3.select(".lnod").select("#unod").append("li").attr("class", "pnod").text(nod.name);
                    } else {
                        d3.select(".lnod").select("#unod").append("li").attr("class", "inod").text(nod.name);
                    }
                    cc++;
                });
                var nam = "";
                $(".pnod, .inod").click(function() {
                    var tag = $(this).text();
                    node[0].forEach(function(nod) {
                        nam = $(nod).text();
                        // console.log(find+" # "+nam);
                        if (tag == nam) {
                             console.log("R tag name 10")
                            $(nod).attr("r", 10);
                        } else {
                            $(nod).attr("r", 5);
                        }
                        co++;
                    });
                })

                cc = 0;
                var arrNames = new Array();
                graph.nodes.forEach(function(nod) {
                    arrNames[cc] = nod.name;
                    cc++;
                });

                $("#search").autocomplete({
                    source: arrNames
                });

                var find = "";
                var co = 0;

                $("#search").keydown(function() {
                    find = $("#search").val();
                     ///console.log(node[0][0].);
//                    console.log("wtf: "+$(nod));
                    node[0].forEach(function(nod) {
                        //console.log("wtf: "+nod[0]);
                        nam = $(nod).text();
                        // console.log(find+" # "+nam);
                        if (find == nam) {
                            $(nod).attr("r", 10);
                        } else {
                            $(nod).attr("r", $(nod).r);
                        }
                        co++;
                    });
                });


                $(".node").click(function() {
                    $(".tag").remove();

                    $("#sNav_rigth").show();
                    var name = $(this).text();

                    graph.nodes.forEach(function(nod) {
                        if (name == nod.name) {
                            console.log("data: "+JSON.stringify(nod));
                            click(nod);
                        }
                    });
                })

                var arrNamesS = new Array();
                var arrNamesT = new Array();

                $(function() {
                    var moveLeft = 20;
                    var moveDown = 10;

                    $(".node").hover(function(e) {
                        if ($(this).attr("class") == "node") {
                            $('.pop').show()
                                    .css('top', e.pageY + moveDown)
                                    .css('left', e.pageX + moveLeft)
                                    .appendTo('body');

                            var name = $(this).text();
                            //var name = document.getElementsByClassName('.node')[this].innerText
                            var index = "";

                            var contS = 0, contT = 0;

                            graph.nodes.forEach(function(nod) {
                                if (name == nod.name) {
                                    index = nod.index;
                                }
                            });

                            var cf = "";
                            var arrChildColors = new Array();
                            graph.links.forEach(function(lk) {
                                if (lk.source.index == index) {
                                    var nameP = "";
                                    var groupP = 0;

                                    nameP = lk.target.name;
                                    groupP = lk.target.group;
                                    $('.node').each(function() {
                                        if ($(this).text() == nameP) {
                                            cf = $(this).css("fill");
                                        }
                                    });

                                    arrNamesS[contS] = nameP + ", " + groupP + ", ";
                                    contS++;
                                } else if (lk.target.index == index) {
                                    var nameC = lk.source.name;
                                    var groupC = lk.source.group;

                                    arrNamesT[contT] = nameC + ", " + groupC + ", ";

                                    $('.node').each(function() {
                                        if ($(this).text() == nameC) {
                                            arrChildColors[contT] = $(this).css("fill");
                                        }
                                    });
                                    contT++;
                                }
                            });

                            pop.append("p").attr("class", "title").text("Nombre: " + name);
                            arrNamesS.forEach(function(arr) {
                                pop.append("p").attr("class", "parent").text(arr).append("div").attr("class", "cir").style("background", cf);
                            });

                            if (arrNamesT.length != 0) {
                                pop.append("p").attr("class", "title").text("Nodos Hijos:");
                                var cc = 0;
                                arrNamesT.forEach(function(arr) {
                                    pop.append("p").attr("class", "child").text(arr).append("div").attr("class", "cir").style("background", arrChildColors[cc]);
                                    cc++;
                                });
                            }
                        }
                    }, function() {
                        $('.pop').hide();
                        arrNamesS = [];
                        arrNamesT = [];
                        $(".parent").remove();
                        $(".child").remove();
                        $(".title").remove();
                    });

                    $(".node").mousemove(function(e) {
                        $(".pop").css('top', e.pageY + moveDown).css('left', e.pageX + moveLeft);
                    });
                });

            });

            node.append("title").text(function(d) {
                return d.name;
            });

            force.on("tick", function() {
                link.attr("x1", function(d) {
                    return d.source.x;
                })
                        .attr("y1", function(d) {
                    return d.source.y;
                })
                        .attr("x2", function(d) {
                    return d.target.x;
                })
                        .attr("y2", function(d) {
                    return d.target.y;
                });

                node.attr("cx", function(d) {
                    return d.x;
                })
                        .attr("cy", function(d) {
                    return d.y;
                });
            });
        }
    });
}