/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


$(function() {
    $("#slider").slider({
        min: 0,
        max: 84,
        value: 0,
        change: function(event, ui) {
            month = 1 + ui.value % 12;
            year = 2008 + Math.floor(ui.value / 12);
            window.location.href = window.location.href.split("#")[0] + "#" + month + "-" + year;
            showReddit();
        },
        create: function(event, ui) {
            if (window.location.href.indexOf("#") > 0) {
                var date = window.location.href.split("#")[1].split("-");
                month = parseInt(date[0]);
                year = parseInt(date[1]);

                var sliderValue = (year - 2006) * 12 + (month - 1);
                $(this).slider("value", sliderValue);
                //month = 1 + ui.value % 12;
                //year = 2006 + Math.floor(ui.value / 12);
            }
            else {
                $(this).slider("value", 0);
            }
        }
    })
            .each(function() {

        //
        // Add labels to slider whose values 
        // are specified by min, max
        //

        // Get the options for this slider (specified above)
        var opt = $(this).data().uiSlider.options;

        // Get the number of possible values
        var vals = opt.max - opt.min;

        // Position the labels
        for (var i = 0; i <= vals; i++) {

            // Create a new element and position it with percentages
            if (i % 12 == 0) {
                var el = $("<label>" + String(2006 + Math.floor(i / 12)) + "</label>")
                        .css("position", "absolute")
                        .css("left", (i / vals * 100) - 2 + "%")
                        .css("top", "15px");
            }

            // Add the element inside #slider
            $("#slider").append(el);
        }
    });
    ;
});

                var w = window,
                d = document,
                e = d.documentElement,
                g = d.getElementsByTagName('body')[0],
                x = w.innerWidth || e.clientWidth || g.clientWidth,
                y = w.innerHeight || e.clientHeight || g.clientHeight;

        var width = 900; //x - 20,
        height = 600; //y - 150;
        padding = 40, // separation between same-color circles
                clusterPadding = 20, // separation between different-color circles
                maxRadius = 0;

        // network objects
        var nodes, links, texts;

        // size of node labels scaled by current zoom
        var textSizeScaled = 30.0;

        var monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"];

        var year = 2006;
        var month = 1;

        var force = d3.layout.force()
                .charge(function(d) {
            return -d.size;
        })
                .linkDistance(function(d) {
            if (year < 2008)
                return (1.0 / d.weight) * 250.0;
            else
                return (1.0 / d.weight);
        })
                .linkStrength(function(d) {
            return d.weight;
        })
                .gravity(1)
                .size([width / 2, height / 2]);

        var color = d3.scale.category20();

        function showReddit() {

            document.getElementById("reddit-date").innerHTML = "reddit in " + monthNames[month - 1] + " " + String(year);

            force.stop();

            if (d3.select("body").select("svg") != null)
                d3.select("body").select("svg").remove();

            var initialScale = 1,
                    initialTranslateX = width / 4,
                    initialTranslateY = height / 4;

            if (year >= 2012) {
                initialScale = 0.3;
                initialTranslateX = width / 2.25;
                initialTranslateY = height / 2.25;
            }

            else if (year >= 2011) {
                initialScale = 0.4;
                initialTranslateX = width / 2.5;
                initialTranslateY = height / 2.5;
            }

            else if (year >= 2010 && month >= 5) {
                initialScale = 0.5;
                initialTranslateX = width / 2.75;
                initialTranslateY = height / 2.75;
            }

            else if (year >= 2009) {
                initialScale = 0.7;
                initialTranslateX = width / 3;
                initialTranslateY = height / 3;
            }

            var zoom = d3.behavior.zoom()
                    //.translate([initialTranslateX, initialTranslateY])
                    //.scale(initialScale)
                    .scaleExtent([0.3, 6])
                    .on("zoom", zoomGraph);

            var svg = d3.select("body").append("svg")
                    .attr("width", width)
                    .attr("height", height)
                    //.attr("transform", "translate(" + 100 + "," + 100 + ")")
                    //.style("background-color", "#3A3A3A")
                    .style("background-color", "#45505c")
                    .append("g")
                    .call(zoom)
                    .attr("transform", "translate(" + initialTranslateX + "," + initialTranslateY +
                    ")scale(" + initialScale + "," + initialScale + ")")
                    .append("g");

            var scale = Math.min(1.0, Math.max(1.5, zoom.scale()));
            textSizeScaled = 30.0 * 1.0 / scale;

            function zoomGraph() {
                svg.attr("transform", "translate(" + d3.event.translate + ")scale(" + d3.event.scale + ")");
                var scale = Math.min(1.0, Math.max(1.5, d3.event.scale));
                textSizeScaled = 30.0 * 1.0 / scale;
                if (texts != null)
                    texts.style("font-size", textSizeScaled);
            }

            svg.append("rect")
                    .attr("class", "overlay")
                    .attr("width", 4 * width)
                    .attr("height", 4 * height);

            svg.selectAll(".node")
                    .data([])
                    .exit().remove();
            svg.selectAll("text")
                    .data([])
                    .exit().remove();
            svg.selectAll(".link")
                    .data([])
                    .exit().remove();

             //d3.json("data/reddit-network-" + String(year) + "-" + String(month) + ".json", function(error, graph) {
            //var graph = $("#datason").text();
            d3.json("reddit.json", function(error, graph) {

                var numClusters = 0;
                var numNodes = 0;
                var clusters = [];
                var biggestNode;
                var linked = {};

                links = svg.selectAll(".link")
                        .data(graph.links)
                        .enter().append("path") //("line")
                        .attr("class", "link")
                        .attr("strength", function(d) {
                    return d.weight;
                })
                        .style("stroke-width", function(d) {
                    return d.weight;
                })
                        .style("fill", "none")
                        .style("stroke-color", "white");

                nodes = svg.selectAll(".node")
                        .data(graph.nodes)
                        .enter().append("circle")
                        .attr("class", "node")
                        .attr("r", function(d) {
                    return Math.log(d.size);
                })
                        .style("fill", function(d) {
                    return color(d.cluster);
                })
                        .style("stroke", function(d) {
                    return color(d.cluster);
                })
                        .on("mouseover", nodeMouseOver)
                        .on("mouseout", nodeMouseOut);

                function nodeMouseOver(d) {
                    d3.select(".nodelabel" + d.name).style("visibility", "visible");

                    nodes.filter(function(o) {
                        return d.cluster != o.cluster;/*!linked[d.id + "," + o.id] && d.id != o.id;*/
                    }).style("visibility", "hidden");
                    texts.filter(function(o) {
                        return d.cluster != o.cluster;/*!linked[d.id + "," + o.id] && d.id != o.id;*/
                    }).style("visibility", "hidden");
                    texts.filter(function(o) {
                        return d.cluster == o.cluster;/*linked[d.id + "," + o.id] || d.id == o.id;*/
                    }).style("visibility", "visible");
                    links.filter(function(o) {
                        return o.target.cluster != d.cluster || o.source.cluster != d.cluster;/*o.target.id != d.id && o.source.id != d.id;*/
                    }).style("visibility", "hidden");
                }

                function nodeMouseOut(d) {
                    nodes.style("visibility", "visible");
                    links.style("visibility", "visible");
                    texts.style("visibility", function(d) {
                        if (d.size >= 0.5 * biggestNode.size || d.name == clusters[d.cluster].name || numNodes <= 20) {
                            return "visible";
                        }
                        else {
                            return "hidden";
                        }
                    });

                    if (d.size < 0.2 * biggestNode.size && d.name != clusters[d.cluster].name && numNodes > 20) {
                        d3.select(".nodelabel" + d.name).style("visibility", "hidden");
                    }
                }

                graph.nodes.forEach(function(d, i) {
                    numNodes += 1;

                    if (d.r > maxRadius) {
                        maxRadius = d.r;
                    }

                    if (!(d.cluster in clusters)) {
                        numClusters += 1;
                        clusters.push(d.cluster);
                    }
                });

                // The largest node for each cluster
                var clusters = new Array(numClusters);
                graph.nodes.forEach(function(d, i) {
                    if (!clusters[d.cluster] || (d.size > clusters[d.cluster].size))
                        clusters[d.cluster] = d;
                    if (!biggestNode || (d.size > biggestNode.size))
                        biggestNode = d;
                });

                // Compute which nodes are connected
                graph.links.forEach(function(d, i) {
                    linked[String(d.source + "," + d.target)] = linked[String(d.target + "," + d.source)] = true;
                });

                texts = svg.selectAll("text.label")
                        .data(graph.nodes)
                        .enter().append("text")
                        .attr("class", function(d) {
                    return "nodelabel" + d.name;
                })
                        .attr("fill", "white")
                        .style("font-size", textSizeScaled)
                        .style("cursor", "default")
                        .style("-webkit-user-select", "none")
                        .style("-webkit-touch-callout", "none")
                        .style("-moz-user-select", "none")
                        .style("-ms-user-select", "none")
                        .style("-khtml-user-select", "none")
                        .style("user-select", "none")
                        .style("visibility", function(d) {
                    if (d.size >= 0.5 * biggestNode.size || d.name == clusters[d.cluster].name || numNodes <= 20) {
                        return "visible";
                    }
                    else {
                        return "hidden";
                    }
                })
                        .on("mouseover", nodeMouseOver)
                        .on("mouseout", nodeMouseOut)
                        .text(function(d) {
                    return d.name;
                });

                // Move d to be adjacent to the cluster node.
                function cluster(alpha) {
                    return function(d) {
                        var cluster = clusters[d.cluster];
                        if (cluster.name == d.name)
                            return;
                        var x = d.x - cluster.x,
                                y = d.y - cluster.y,
                                l = Math.sqrt(x * x + y * y),
                                r = Math.log(d.size) + Math.log(cluster.size);
                        if (l != r) {
                            l = (l - r) / l * alpha;
                            d.x -= x *= l;
                            d.y -= y *= l;
                            cluster.x += x;
                            cluster.y += y;
                        }
                    };
                }

                // Resolves collisions between d and all other circles.
                function collide(alpha, realAlpha) {
                    if (realAlpha < 0.01)
                        return;

                    var quadtree = d3.geom.quadtree(nodes);
                    return function(d) {
                        var r = d.r + maxRadius + Math.max(padding, clusterPadding),
                                nx1 = d.x - r,
                                nx2 = d.x + r,
                                ny1 = d.y - r,
                                ny2 = d.y + r;
                        quadtree.visit(function(quad, x1, y1, x2, y2) {
                            if (quad.point && (quad.point !== d)) {
                                var x = d.x - quad.point.x,
                                        y = d.y - quad.point.y,
                                        l = Math.sqrt(x * x + y * y),
                                        r = d.r + quad.point.radius + (d.cluster === quad.point.cluster ? padding : clusterPadding);
                                if (l < r) {
                                    l = (l - r) / l * alpha;
                                    d.x -= x *= l;
                                    d.y -= y *= l;
                                    quad.point.x += x;
                                    quad.point.y += y;
                                }
                            }
                            return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
                        });
                    };
                }

                force.nodes(graph.nodes)
                        .links(graph.links)
                        .start()
                        .on("tick", tick);
                //for (var i = 150; i > 0; --i) force.tick(d3.event);
                //force.stop();

                function tick(e) {
                    //if (e.alpha < 0.05) force.stop();

                    nodes.each(cluster(10 * e.alpha * e.alpha))
                            //.each(collide(.5, e.alpha))
                            .attr("cx", function(d) {
                        return d.x;
                    })
                            .attr("cy", function(d) {
                        return d.y;
                    });

                    links.attr("d", function(d) {
                        var dx = d.target.x - d.source.x,
                                dy = d.target.y - d.source.y,
                                dr = Math.sqrt(dx * dx + dy * dy);
                        return "M" + d.source.x + "," + d.source.y + "A" + dr + "," + dr + " 0 0,1 " + d.target.x + "," + d.target.y;
                    });

                    texts.attr("transform", function(d) {
                        return "translate(" + d.x + "," + d.y + 100 + ")";
                    })
                }
            });

            month += 1;
            if (month > 12) {
                month = 1;
                year += 1;
            }
        }